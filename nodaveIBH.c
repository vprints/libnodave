/*
 Part of Libnodave, a free communication libray for Siemens S7 300/400.
 This version uses the IBHLink MPI-Ethernet-Adapter from IBH-Softec.
 www.ibh.de
 
 (C) Thomas Hergenhahn (thomas.hergenhahn@web.de) 2002, 2003.

 Libnodave is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 Libnodave is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Libnodave; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  
*/

#include "nodaveIBH.h"
#include "sampIBH.c"
#include <stdio.h>
#include <string.h>
#include "log.h"

int _daveWriteIBH(daveInterface * di, void * buffer, int len) {
    int res;
    if (di==NULL) return -2;
//    if (di->error) return -1;
    res=write(di->fd.wfd, buffer, len);
//    if (res<0) di->error|=1;
    return res;
}    

/*
    Read one complete packet. The 3rd byte contains length information.
*/
int _daveReadIBHPacket(daveInterface * di,uc *b) {
	int i,res=0;
	res+=_daveReadOne(di,b);
	res+=_daveReadOne(di,b+res);
	res+=_daveReadOne(di,b+res);
	if ((res>0)&&(res<3)) res+=_daveReadOne(di,b+res);
	if ((res>0)&&(res<3)) res+=_daveReadOne(di,b+res);
        if (res<3) {
	    if (daveDebug & daveDebugByte) {
		LOG2("res %d ",res);
		_daveDump("readIBHpacket: short packet", b, res);
	    }	
	    return (0); // short packet
	}
/*
    This may be shorter, but we would hang on malformed packets.
	res+=read(fd, b+res,b[2]+5 ); //length +8 -3 already read    
    So do it byte by byte.
    Somebody might have a better solution using non-blocking something.
*/	
	while (res<(b[2]+8)) {
	    i=_daveReadOne(di, b+res); //length +8 -3 already read
	    if (i<1) return (0);
	    res+=i;
	} 
	if (daveDebug & daveDebugByte) 
	    LOG3("readIBHpacket: %d bytes read, %d needed\n",res,b[2]+8);
	return (res);
};

/*
    This performs initialization steps with sampled byte sequences. If chal is <>NULL
    it will send this byte sequence.
    It will then wait for a packet and compare it to the sample.
*/
int _daveInitStepIBH(daveInterface * iface, uc * chal, int cl, us* resp,int rl, uc*b) {
    int res,a=0;
    LOG1("_daveInitStepIBH before write.\n");
    if (chal) res=_daveWriteIBH(iface, chal, cl);
    LOG2("_daveInitStepIBH write returned %d.\n",res);
    if (res<0) return 100;
    res=_daveReadIBHPacket(iface, b);
/*
    We may get a network layer ackknowledge and an MPI layer ackknowledge, which we discard.
    So, normally at least the 3rd packet should have the desired response. 
    Waiting for more does:
	-discard extra packets resulting from last step.
	-extend effective timeout.
*/    
    while (a<5) {
	if (a) {
	    res=_daveReadIBHPacket(iface, b);
//#include "dumpchars.cc"    	    
	}
	if (res>0) {
	    if (0==_daveMemcmp(resp,b,rl/2)) {
		if (daveDebug & daveDebugInitAdapter) 
		    LOG3("*** Got response %d %d\n",res,rl);
		return a;
	    }  else {	
		if (daveDebug & daveDebugInitAdapter)  LOG1("wrong!\n");
	    }
	}
	a++;
    }
    return a;
}

/*
    Connect to a PLC via IBH-NetLink. Returns 0 for success and a negative number on errors.
*/
int daveConnectPLC_IBH(daveConnection*dc) {
    int a;
    uc b[daveMaxRawLen];
    dc->iface->timeout=500000;
    a=_daveInitStepIBH(dc->iface, chal0,sizeof(chal0),resp0,sizeof(resp0),b);
    LOG2("_daveInitStepIBH 1:%d\n",a); if (a) return -1;
    a=_daveInitStepIBH(dc->iface, chal1,sizeof(chal1),resp1,/*sizeof(resp1)*/16,b);
    LOG2("_daveInitStepIBH 2:%d\n",a); if (a>3) return -2;
    a=_daveInitStepIBH(dc->iface, chal2,sizeof(chal2),resp2,sizeof(resp2),b);
    LOG2("_daveInitStepIBH 3:%d\n",a); if (a>3) return -3;
    dc->templ.localMPI=0;
    dc->templ.src_conn=20;
    dc->templ.dst_conn=20;

    daveDisconnectPLC_IBH(dc);	// this may help when last session didn't end properly
				// to do: find out whether it will shutdown third party's
				// connections, too
    chal3[8]=dc->templ.src_conn;
    
    a=_daveInitStepIBH(dc->iface, chal3,sizeof(chal3),resp3,sizeof(resp3),b);
    if (b[9]!=0) {
	LOG1("trying next ID:\n");
	dc->templ.src_conn++;
	chal3[8]=dc->templ.src_conn;
	a=_daveInitStepIBH(dc->iface, chal3,sizeof(chal3),resp3,sizeof(resp3),b);
    }	
    LOG2("_daveInitStepIBH 4:%d\n",a); if (a>3) /* !!! */ return -4;;
    
    chal8[8]=dc->templ.src_conn;
    a=_daveInitStepIBH(dc->iface, chal8,sizeof(chal8),resp7,sizeof(resp7),b);
    dc->templ.dst_conn=b[9];
    LOG3("_daveInitStepIBH 5:%d connID: %d\n",a, dc->templ.dst_conn); if (a>3) return -5;
    
    chal011[8]=dc->templ.src_conn;
    chal011[9]=dc->templ.dst_conn;
    a=_daveInitStepIBH(dc->iface, chal011,sizeof(chal011),resp09,sizeof(resp09),b);
    dc->templ.dst_conn=b[9];
    LOG3("_daveInitStepIBH 5a:%d connID: %d\n",a, dc->templ.dst_conn); if (a>3) return -5;
    
    chal11[8]=dc->templ.src_conn;
    chal11[9]=dc->templ.dst_conn;
    a=_daveInitStepIBH(dc->iface, chal11,sizeof(chal11),resp10,sizeof(resp10),b);
    LOG2("_daveInitStepIBH 6:%d \n",a); if (a>5)/*!!!*/ return -6;

    dc->packetNumber=4;
    dc->packetNumber++;
    dc->templ.packetNumber=1;
    
    MPIack[8]=dc->templ.src_conn;
    MPIack[9]=dc->templ.dst_conn;
    _daveWriteIBH(dc->iface, MPIack, sizeof(MPIack));
    dc->iface->timeout=100000;
    return (0);
}    

/*
    Disconnect from a PLC via IBH-NetLink. 
    Returns 0 for success and a negative number on errors.
*/
int daveDisconnectPLC_IBH(daveConnection*dc) {
    uc b[daveMaxRawLen];
    chal11[8]=dc->templ.src_conn;
    chal11[9]=dc->templ.dst_conn;
    
    _daveWriteIBH(dc->iface, chal31, sizeof(chal31));
    _daveReadIBHPacket(dc->iface, b);
    _daveWriteIBH(dc->iface, chal311, sizeof(chal311));
    _daveReadIBHPacket(dc->iface, b);
    _daveWriteIBH(dc->iface, chal34, sizeof(chal34));
    _daveReadIBHPacket(dc->iface, b);
    _daveReadIBHPacket(dc->iface, b);
    return 0;
}   

int _davePackPDU(daveConnection * dc,PDU *p1) {
    IBHpacket * p2;
    uc * packet=dc->msgOut;
    MPIheader * hm= (MPIheader*) (packet+sizeof(IBHpacket)); // MPI headerPDU begins packet header
    memcpy(hm,&(dc->templ),sizeof(MPIheader));	// copy MPI header from template
    hm->len=2+p1->hlen+p1->plen+p1->dlen;		// set MPI length
    hm->func=0xf1;				// set MPI "function code"
    p2= (IBHpacket*) dc->msgOut;	
    p2->ch1=7;
    p2->ch2=0xff;
    p2->len=hm->len+5;
    p1->header[4]=1+hm->packetNumber;		// give the PDU a number
    p2->packetNumber=dc->packetNumber;
    dc->packetNumber++;
    p2->rFlags=0x82;
    p2->sFlags=0;
    dc->templ.packetNumber++; 
    if(dc->templ.packetNumber==0)dc->templ.packetNumber=1;
    return 0;
}    

/*
    Read len bytes from PLC memory area "area", data block DBnum. 
    If a buffer pointer is provided, data will be copied into this buffer.
    If it's NULL you can get your data from the dataPointer in daveConnection long
    as you do not send further requests.
*/
int daveReadBytesIBH(daveConnection * dc,int area, int DBnum, int start,int len, void * buffer) {
    int res, count;
    PDU p1;
//    IBHpacket * p2;
//    uc * packet=dc->msgOut;
    MPIheader * hm= (MPIheader*) (dc->msgOut+sizeof(IBHpacket)); // MPI headerPDU begins packet header
    p1.header=((uc*)hm)+sizeof(MPIheader);	// PDU begins after MPI header
//    _daveConstructReadRequest(&p1, area, DBnum, start, len);
    davePrepareReadRequest(dc, &p1);
    daveAddVarToReadRequest(&p1, area, DBnum, start, len);
    if (daveDebug & daveDebugPDU) _daveDumpPDU(&p1);
    _davePackPDU(dc, &p1);
    res=_daveWriteIBH(dc->iface, dc->msgOut, dc->msgOut[2]+8);
    if (daveDebug & daveDebugPDU)    
	_daveDump("I send request: ",dc->msgOut, dc->msgOut[2]+8);
    count=0;
    do {
	dc->AnswLen=_daveReadIBHPacket(dc->iface, dc->msgIn);
	count++;
    } while ((_daveAnalyze(dc)!=_davePtReadResponse)&&(count<4));
    if (count<=4) return dc->AnswLen; else return 0;
}

/*
    Write len bytes to PLC memory area "area", data block DBnum. 
*/
int daveWriteBytesIBH(daveConnection * dc,int area, int DBnum, int start,int len, void * buffer) {
    int res, count;
    PDU p1;
    MPIheader * hm;
//    IBHpacket * p2;
    uc * packet=dc->msgOut;
    hm= (MPIheader*) (packet+sizeof(IBHpacket)); // MPI headerPDU begins packet header
    p1.header=((uc*)hm)+sizeof(MPIheader);	// PDU begins after MPI header
// construct response:	    
    davePrepareWriteRequest(dc, &p1);
    daveAddVarToWriteRequest(&p1, area, DBnum, start, len, buffer);
//    _daveConstructWriteRequest(&p1, area, DBnum, start, len, buffer);
    if (daveDebug & daveDebugPDU)
	_daveDumpPDU(&p1);
    _davePackPDU(dc, &p1);
    res=_daveWriteIBH(dc->iface, packet, packet[2]+8);
    if (daveDebug & daveDebugPDU)    
	_daveDump("I send request: ",packet, packet[2]+8);
    count=0;
    do {
	dc->AnswLen=_daveReadIBHPacket(dc->iface, dc->msgIn);
	count++;
    } while ((_daveAnalyze(dc)!=_davePtWriteResponse)&&(count<4));
    if (count<=4) return 0; else return -1;
}

void _daveSendMPIAck_IBH(daveConnection*dc) {
    MPIack[15]=dc->msgIn[16];
    MPIack[8]=dc->templ.src_conn;
    MPIack[9]=dc->templ.dst_conn;
    _daveWriteIBH(dc->iface,MPIack,sizeof(MPIack));
}
/**
    PDU handling:
**/

uc MPIconnectResponse[]={ 
	0xff,0x07,0x13,0x00,0x00,0x00,0xc2,0x02, 0x14,0x14,0x03,0x00,0x00,0x22,0x0c,0xd0,
	0x04,0x00,0x80,0x00,0x02,0x00,0x02,0x01, 0x00,0x01,0x00,
};


/*
    packet analysis. mixes all levels.
*/
int _daveAnalyze(daveConnection * dc) {
    int haveResp;
    IBHpacket * p, *p2;
    MPIheader * pm;
    MPIheader2 * m2;
    PDU p1,pr;
    uc resp[2000];
    
    if (dc->AnswLen==0) return _davePtEmpty;
    haveResp=0;
    
    p= (IBHpacket*) dc->msgIn;
    dc->needAckNumber=-1;		// Assume no ack
    if (daveDebug & daveDebugPacket){
	LOG2("Channel: %d\n",p->ch1);
	LOG2("Channel: %d\n",p->ch2);
        LOG2("Length:  %d\n",p->len);
	LOG2("Number:  %d\n",p->packetNumber);
        LOG3("sFlags:  %04x rFlags:%04x\n",p->sFlags,p->rFlags);
    }    
    if (p->rFlags==0x82) {
	pm= (MPIheader*) (dc->msgIn+sizeof(IBHpacket));
	if (daveDebug & daveDebugMPI){    
	    LOG2("srcconn: %d\n",pm->src_conn);
	    LOG2("dstconn: %d\n",pm->dst_conn);
	    LOG2("MPI:     %d\n",pm->MPI);
	    LOG2("MPI len: %d\n",pm->len);
	    LOG2("MPI func:%d\n",pm->func);
	}    
	if (pm->func==0xf1) {
	    if (daveDebug & daveDebugMPI)    
		LOG2("MPI packet number: %d needs ackknowledge\n",pm->packetNumber);
	    dc->needAckNumber=pm->packetNumber;
//	    p1.header=((uc*)pm)+sizeof(MPIheader);
	    dc->PDUstartI= sizeof(IBHpacket)+sizeof(MPIheader);
	    _daveSetupReceivedPDU(dc, &p1);
// construct response:	    
	    pr.header=resp+sizeof(IBHpacket)+sizeof(MPIheader2);
	    p2= (IBHpacket*) resp;
	    p2->ch1=p->ch2;
	    p2->ch2=p->ch1;
	    p2->packetNumber=0;
	    p2->sFlags=0;
	    p2->rFlags=0x2c2;
	    
	    m2= (MPIheader2*) (resp+sizeof(IBHpacket));
	    m2->src_conn=pm->src_conn;
	    m2->dst_conn=pm->dst_conn;
	    m2->MPI=pm->MPI;
	    m2->xxx1=0;
	    m2->xxx2=0;
	    m2->xx22=0x22;
	    if (p1.param[0]==daveFuncRead) {
#ifdef passiveMode	    
		_daveHandleRead(&p1,&pr);
		haveResp=1;
#endif
		m2->len=pr.hlen+pr.plen+pr.dlen+2;
		p2->len=m2->len+7;
	    } else if (p1.param[0]==daveFuncWrite) {
#ifdef passiveMode	    
		_daveHandleWrite(&p1,&pr);
		haveResp=1;
#endif		
		m2->len=pr.hlen+pr.plen+pr.dlen+2;
		p2->len=m2->len+7;
	    } else {
		if (daveDebug & daveDebugPDU)    
		    LOG2("Unsupported PDU function code: %d\n",p1.param[0]);
		return _davePtUnknownPDUFunc;
	    }
	    
	}
	if (pm->func==0xb0) {
    	    LOG2("Ackknowledge for packet number: %d\n",*(dc->msgIn+15));
	    return _davePtMPIAck;
	}
	if (pm->func==0xe0) {
	    LOG2("Connect to MPI: %d\n",pm->MPI);
	    memcpy(resp, MPIconnectResponse, sizeof(MPIconnectResponse));
	    resp[8]=pm->src_conn;
	    resp[9]=pm->src_conn;
	    resp[10]=pm->MPI;
	    haveResp=1;
	}    
    }	
    
    if (p->rFlags==0x2c2) {
	MPIheader2 * pm= (MPIheader2*) (dc->msgIn+sizeof(IBHpacket));
	if (daveDebug & daveDebugMPI) {
	    LOG2("srcconn: %d\n",pm->src_conn);
	    LOG2("dstconn: %d\n",pm->dst_conn);
	    LOG2("MPI:     %d\n",pm->MPI);
	    LOG2("MPI len: %d\n",pm->len);
	    LOG2("MPI func:%d\n",pm->func);
	}
	if (pm->func==0xf1) {
	    if (daveDebug & daveDebugMPI)    
		LOG2("MPI packet number: %d\n",pm->packetNumber);
	    dc->needAckNumber=pm->packetNumber;
//	    p1.header=((uc*)pm)+sizeof(MPIheader2);
	    dc->PDUstartI= sizeof(IBHpacket)+sizeof(MPIheader2);
	    _daveSetupReceivedPDU(dc, &p1);
	    
	    if (p1.param[0]==daveFuncRead) {
		LOG1("read Response\n");
		 _daveSendMPIAck_IBH(dc);
		 dc->resultPointer=p1.data+4;
		 dc->_resultPointer=p1.data+4;
		 dc->AnswLen=p1.dlen-4;
		return _davePtReadResponse;
	    } else if (p1.param[0]==daveFuncWrite) {
		 _daveSendMPIAck_IBH(dc);
		 LOG1("write Response\n");
		 return _davePtWriteResponse;
	    } else {
		LOG2("Unsupported PDU function code: %d\n",p1.param[0]);
	    }
	    
	}
	if (pm->func==0xb0) {
	    if (daveDebug & daveDebugMPI)    
    		LOG2("Ackknowledge for packet number: %d\n",pm->packetNumber);
	} else {
	    LOG2("Unsupported MPI function code !!: %d\n",pm->func);
	    _daveSendMPIAck_IBH(dc);
	}
    }
    if ((p->rFlags==0x82)&&(p->packetNumber)&&(p->len)) _daveSendIBHNetAck(dc);
    if (haveResp) {
	_daveWriteIBH(dc->iface, resp, resp[2]+8);
	_daveDump("I send response:", resp, resp[2]+8);
    }	
    return 0;
};

/**
    Utillities:
**/
/*
    send a network level ackknowledge
*/
void _daveSendIBHNetAck(daveConnection * dc) {
//    0xff,0x07,0x05,0x02,0x82,0x00,0x00,0x00, 0x14,0x00,0x03,0x01,0x09,
    IBHpacket * p;
    us d;
    uc c;
    uc ack[13];
    memcpy(ack, dc->msgIn, sizeof(ack));
    p= (IBHpacket*) ack;
    p->sFlags; p->sFlags=p->rFlags; p->rFlags=d;
    p->ch1; p->ch1=p->ch2; p->ch2=c;		// certainly nonsense, but I cannot test it
						// at the moment, and because it DID work,
						// I'll leave it as it is.
    p->len=sizeof(ack)-sizeof(IBHpacket);
    ack[11]=1;
    ack[12]=9;
//    LOG2("Sending net level ack for number: %d\n",p->packetNumber);
    _daveWriteIBH(dc->iface, ack,sizeof(ack));
}

#ifdef passiveMode

void _daveSendMPIAck2(daveConnection *dc) {
//	0xff,0x07,0x0a,0x00,0x00,0x00,0xc2,0x02, 0x14,0x14,0x03,0x00,0x00,0x22,0x03,0xb0,
//	0x01,0x03,
    IBHpacket * p;
    uc c;
    uc ack[18];
    memcpy(ack,dc->msgIn,sizeof(ack));
    p= (IBHpacket*) ack;
//    us d=p->sender; p->sender=p->receiver; p->receiver=d;
    p->rFlags|=0x240;	//Why that?
    c=p->ch1; p->ch1=p->ch2; p->ch2=c;
    p->len=sizeof(ack)-sizeof(IBHpacket);
    p->packetNumber=0;	// this may mean: no net work level acknowledge
    ack[13]=0x22;
    ack[14]=3;
    ack[15]=176;
    ack[16]=1;
    ack[17]=dc->needAckNumber;
    _daveDump("send MPI-Ack2",ack,sizeof(ack));
    _daveWriteIBH(dc->iface,ack,sizeof(ack));
};

#endif
/*
    Changes:
    09/18/2003	Send acknowledge for unsupported MPI function. This seems to help when the IBH 
		NetLink sometimes sends a packet with MPI func 0x80. The former version hang up 
		in this situation.
    05/21/2004	Use some more common functions from nodave.		
    07/19/2004	removed some unused vars
*/    
