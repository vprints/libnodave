/*
 Test and demo program for Libnodave, a free communication libray for Siemens S7.
 
 **********************************************************************
 * WARNING: This and other test programs overwrite data in your PLC.  *
 * DO NOT use it on PLC's when anything is connected to their outputs.*
 * This is alpha software. Use entirely on your own risk.             * 
 **********************************************************************
 
 (C) Thomas Hergenhahn (thomas.hergenhahn@web.de) 2002, 2003.

 This is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Libnodave; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "nodave.h"
#include "openSocket.h"
#include <fcntl.h>

#ifdef LINUX
#include <unistd.h>
#include <fcntl.h>
#define UNIX_STYLE
#endif

#ifdef CYGWIN
#include <unistd.h>
#define UNIX_STYLE
#endif

#ifdef BCCWIN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <time.h>
#define WIN_STYLE
#endif

void usage()
{
    printf("Usage: runStopMPI [-d] [-w] serial port.\n");
    printf("-d will produce a lot of debug messages.\n");
    printf("-r will put your PLC in RUN mode.\n");
    printf("-s will put your PLC in STOP mode.\n");
    printf("-2 uses a slightly different version of the MPI protocol. Try it, if your Adapter doesn't work.\n");
    printf("Example: runStopMPI -w /dev/ttyS0\n");
}

void wait() {
    uc c;
    printf("Press return to continue.\n");
    read(0,&c,1);
}    

    uc paMakeRun[]= {
	0x28,0,0,0,0,0,0,0xFD,0,0,9,'P','_','P','R','O','G','R','A','M'
    };
    
    uc paMakeStop[]= {
	0x29,0,0,0,0,0,9,'P','_','P','R','O','G','R','A','M'
    };
    
int main(int argc, char **argv) {
    int i, a, b, c, adrPos, doRun, doStop, res, useProto;
    PDU p,p2;
    float d;
    daveInterface * di;
    daveConnection * dc;
    _daveOSserialType fds;
    
    adrPos=1;
    doRun=0;
    doStop=0;
    
    useProto=daveProtoISOTCP;

    if (argc<2) {
	usage();
	exit(-1);
    }    

    while (argv[adrPos][0]=='-') {
	if (strcmp(argv[adrPos],"-d")==0) {
	    daveDebug=daveDebugAll;
	} else
	if (strcmp(argv[adrPos],"-r")==0) {
	    doRun=1;
	} else
	if (strcmp(argv[adrPos],"-s")==0) {
	    doStop=1;
	}
	else if (strcmp(argv[adrPos],"-2")==0) {
	    useProto=daveProtoMPI2;
	} 
	adrPos++;
	if (argc<=adrPos) {
	    usage();
	    exit(-1);
	}
    }    
    if (0==(doStop+doRun)) {
        usage();
    }
    fds.rfd=openSocket(102, argv[adrPos]);
    fds.wfd=fds.rfd;
    if (fds.rfd>0) { 
	di =daveNewInterface(fds, "IF1", 0, useProto, daveSpeed187k);
	dc =daveNewConnection(di,2,0,2);  // insert your MPI address here
	printf("ConnectPLC\n");
	if (0==daveConnectPLC(dc)) {;
	printf("Trying to read 16 bytes from FW0.\n");
	wait();
	daveReadBytes(dc,daveFlags,0,0,16,NULL);
        a=daveGetDWORD(dc);
        b=daveGetDWORD(dc);
        c=daveGetDWORD(dc);
        d=daveGetFloat(dc);
	printf("FD0: %d\n",a);
	printf("FD4: %d\n",b);
	printf("FD8: %d\n",c);
	printf("FD12: %f\n",d);
	if(doStop) {
	    p.header=dc->msgOut+dc->PDUstartO;
	    _daveInitPDUheader(&p, 1);
	    _daveAddParam(&p, paMakeStop, sizeof(paMakeStop));
	    res=_daveExchange(dc, &p);
	    if (res==daveResOK) {
		res=_daveSetupReceivedPDU(dc,&p2);
		if (daveDebug & daveDebugPDU) {
		    _daveDumpPDU(&p2);
		}
	    }
	} // doStop
	if(doRun) {
	    p.header=dc->msgOut+dc->PDUstartO;
	    _daveInitPDUheader(&p, 1);
	    _daveAddParam(&p, paMakeRun, sizeof(paMakeRun));
	    res=_daveExchange(dc, &p);
	    if (res==daveResOK) {
		res=_daveSetupReceivedPDU(dc, &p2);
		if (daveDebug & daveDebugPDU) {
		    _daveDumpPDU(&p2);
		}
	    }
	} // doRun
	printf("Now disconnecting\n");	
	daveDisconnectPLC(dc);
	daveDisconnectAdapter(di);
	return 0;
	} else {
	    printf("Couldn't connect to PLC.\n Please try again. You may also try the option -2 for some adapters.\n");	
	    return -2;
	}
    } else {
	printf("Couldn't open serial port %s\n",argv[adrPos]);	
	return -2;
    }	
}

/*
    Changes: 
    07/19/04 removed unused vars.
    09/09/04 applied patch for variable Profibus speed from Andrew Rostovtsew.
    09/09/04 removed unused includes byteswap.h, sys/time.h
*/
