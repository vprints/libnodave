/*
 Test and demo program for Libnodave, a free communication libray for Siemens S7.
 
 **********************************************************************
 * WARNING: This and other test programs overwrite data in your PLC.  *
 * DO NOT use it on PLC's when anything is connected to their outputs.*
 * This is alpha software. Use entirely on your own risk.             * 
 **********************************************************************
 
 (C) Thomas Hergenhahn (thomas.hergenhahn@web.de) 2002, 2003.

 This is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Libnodave; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "nodave.h"
#include "setport.h"
#include <fcntl.h>

#ifdef LINUX
#include <unistd.h>
#include <fcntl.h>
#define UNIX_STYLE
#endif

#ifdef CYGWIN
#include <unistd.h>
#define UNIX_STYLE
#endif

#ifdef BCCWIN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <time.h>
#define WIN_STYLE
#endif


void usage()
{
    printf("Usage: stopPPI [-d] [-w] serial port.\n");
    printf("-w will try to write to Flag words. It will overwrite FB0 to FB15 (MB0 to MB15) !\n");
    printf("-d will produce a lot of debug messages.\n");
    printf("-b will run benchmarks. Specify -b and -w to run write benchmarks.\n");
    printf("-l will run a test to determine maximum length of a block in read.\n");
    printf("-z will read some SZL list items (diagnostic information).\n");
    printf("Example: stopPPI -w /dev/ttyS0\n");
}

void wait() {
    uc c;
    printf("Press return to continue.\n");
    read(0,&c,1);
}    

void readSZL(daveConnection *dc,int id, int index) {
    int res;
    printf("Trying to read SZL-ID %04X index %02X.\n",id,index);
    res=daveReadSZL(dc,id,index,NULL);
    printf("Function result: %d %s\n",res,daveStrerror(res));
    _daveDump("Data",dc->resultPointer,dc->AnswLen);
}    

//extern 
//int seconds,thirds;

int main(int argc, char **argv) {
    int a, adrPos, doWrite, doBenchmark, doLentest, doSZLread, res;
    daveInterface * di;
    daveConnection * dc;
    _daveOSserialType fds;
    PDU p,p2;

    uc paMakeStop[]= {
	0x29,0,0,0,0,0,9,'P','_','P','R','O','G','R','A','M'
    };
    
    
    adrPos=1;
    doWrite=0;
    doBenchmark=0;
    doLentest=0;
    doSZLread=0;

    if (argc<2) {
	usage();
	exit(-1);
    }    

    while (argv[adrPos][0]=='-') {
	if (strcmp(argv[adrPos],"-d")==0) {
	    daveDebug=daveDebugAll;
	} else
	if (strcmp(argv[adrPos],"-w")==0) {
	    doWrite=1;
	} else
	if (strcmp(argv[adrPos],"-b")==0) {
	    doBenchmark=1;
	} else
	if (strcmp(argv[adrPos],"-l")==0) {
	    doLentest=1;
	}
	if (strcmp(argv[adrPos],"-z")==0) {
	    doSZLread=1;
	} 
	adrPos++;
	if (argc<=adrPos) {
	    usage();
	    exit(-1);
	}	
    }    
    
    fds.rfd=setPort(argv[adrPos],"9600",'E');
    fds.wfd=fds.rfd;
    if (fds.rfd>0) { 
	di =daveNewInterface(fds, "IF1", 0, daveProtoPPI, daveSpeed187k);
	dc =daveNewConnection(di, 2, 0, 0);  // insert your PPI address here
        
//
// just try out what else might be readable in an S7-200 (on your own risk!):
//
	printf("Trying to read 64 bytes (32 words) from data block 1.\n This V memory of the 200.\n");
	wait();
        res=daveReadBytes(dc,daveDB,1,0,64,NULL);
	if (res==0) {
	    a=daveGetWORD(dc);
	    printf("VW0: %d\n",a);
	    a=daveGetWORD(dc);
	    printf("VW2: %d\n...\n",a);
	}
	p.header=dc->msgOut+dc->PDUstartO;
	_daveInitPDUheader(&p, 1);
	_daveAddParam(&p, paMakeStop, sizeof(paMakeStop));
	res=_daveExchange(dc, &p);
	if (res==daveResOK) {
	    res=_daveSetupReceivedPDU(dc, &p2);
	    if (daveDebug & daveDebugPDU) {
		_daveDumpPDU(&p2);
	    }
	}
	return 0;
    } else {
	printf("Couldn't open serial port %s\n",argv[adrPos]);	
	return -1;
    }	
}

/*
    Changes: 
    07/19/04 removed unused vars.
    09/09/04 applied patch for variable Profibus speed from Andrew Rostovtsew.
    09/09/04 removed unused includes byteswap.h, sys/time.h
*/
