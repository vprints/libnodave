/*
 Test and demo program for Libnodave, a free communication libray for Siemens S7.
 This program will read all program and data blocks from your PLC and store them 
 each to a file.
 
 **********************************************************************
 * WARNING: This and other test programs overwrite data in your PLC.  *
 * DO NOT use it on PLC's when anything is connected to their outputs.*
 * This is alpha software. Use entirely on your own risk.             * 
 **********************************************************************
 
 (C) Thomas Hergenhahn (thomas.hergenhahn@web.de) 2002, 2003.

 This is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Libnodave; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  
*/
#include <stdlib.h>
#include <stdio.h>
#include "nodave.h"
#include "setport.h"
#include <string.h>
#include <fcntl.h>

#ifdef LINUX
#include <unistd.h>
#define UNIX_STYLE
#endif

#ifdef CYGWIN
#include <unistd.h>
#define UNIX_STYLE
#endif

#ifdef BCCWIN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define WIN_STYLE

    void usage();
    void wait();
#endif

void wait() {
    uc c;
    printf("Press return to continue.\n");
#ifdef UNIX_STYLE    
    read(0,&c,1);
#endif    
}    

void loadBlocksOfType(daveConnection * dc, int blockType) {
    int j, i, uploadID, len, more;
#ifdef UNIX_STYLE
    int fd;
#endif	        
#ifdef WIN_STYLE	    
    HANDLE fd;
    unsigned long res;
#endif	        
    char blockName [20];
    uc blockBuffer[20000],*bb;
    daveBlockEntry dbe[256];   
    j=daveListBlocksOfType(dc, blockType, dbe);
    printf("%d blocks of type %s\n",j,daveBlockName(blockType));
    for (i=0; i<j; i++) {
	printf("%s%d  %d %d\n",
	    daveBlockName(blockType),
	    dbe[i].number, dbe[i].type[0],dbe[i].type[1]);	
	bb=blockBuffer;
	len=0;
	if (0==initUpload(dc, blockType, dbe[i].number, &uploadID)) {
    	    do {
		doUpload(dc,&more,&bb,&len,uploadID);
	    } while (more);
	    sprintf(blockName,"%s%d.mc7",daveBlockName(blockType), dbe[i].number);	
#ifdef UNIX_STYLE
    	    fd=open(blockName,O_RDWR|O_CREAT|O_TRUNC,0644);
    	    write(fd, blockBuffer, len);
    	    close(fd);
#endif	    
#ifdef WIN_STYLE
    	    fd=open(blockName,O_RDWR|O_CREAT|O_TRUNC,0644);
    	    WriteFile(fd, blockBuffer, len, &res, NULL);
    	    close(fd);
#endif	    
    	    endUpload(dc,uploadID);
	}    
    }
}

void usage()
{
    printf("Usage: readout [-d] [-2] serial port\n");
    printf("-s will read out SFBs and SFCs.\n");
    printf("-2 uses a MPI protocol. You may try it if your adapter doesn't work.\n");
    printf("-d will produce a lot of debug messages.\n");
#ifdef UNIX_STYLE    
    printf("Example: readoutMPI -2 /dev/ttyS0\n");
#endif	        
#ifdef WIN_STYLE    
    printf("Example: readoutMPI -2 COM1\n");
#endif	        
}


int main(int argc, char **argv) {
    int i,j, useProto, adrPos,doWrite, doBenchmark, doLentest,
	doSZLread, doMultiple, doClear, doNewfunctions, doSFBandSFC,
	initSuccess,
	saveDebug,
	res, speed, localMPI, plcMPI;
    char buf [daveOrderCodeSize];
    daveInterface * di;
    daveConnection * dc;
    daveBlockTypeEntry dbl[20];  // I never saw more then 7 entries
    _daveOSserialType fds;
    adrPos=1;
    doWrite=0;
    doBenchmark=0;
    doLentest=0;
    doSZLread=0;
    doMultiple=0;
    doClear=0;
    doNewfunctions=0;
    doSFBandSFC=0;
    
    useProto=daveProtoMPI;
    speed=daveSpeed187k;
    localMPI=0;
    plcMPI=2;
    
    if (argc<2) {
	usage();
	exit(-1);
    }    
    
    while (argv[adrPos][0]=='-') {
	if (strcmp(argv[adrPos],"-d")==0) {
	    daveDebug=daveDebugAll;
	} else
	if (strcmp(argv[adrPos],"-w")==0) {
	    doWrite=1;
	} else
	if (strcmp(argv[adrPos],"-b")==0) {
	    doBenchmark=1;
	} else
	if (strncmp(argv[adrPos],"--local=",8)==0) {
	    localMPI=atol(argv[adrPos]+8);
	    printf("setting local MPI address to:%d\n",localMPI);
	} else
	if (strncmp(argv[adrPos],"--mpi=",6)==0) {
	    plcMPI=atol(argv[adrPos]+6);
	    printf("setting MPI address of PLC to:%d\n",plcMPI);
	} else
	if (strcmp(argv[adrPos],"-l")==0) {
	    doLentest=1;
	}
	else if (strcmp(argv[adrPos],"-z")==0) {
	    doSZLread=1;
	}
	else if (strcmp(argv[adrPos],"-m")==0) {
	    doMultiple=1;
	}
	else if (strcmp(argv[adrPos],"-c")==0) {
	    doClear=1;
	}
	else if (strcmp(argv[adrPos],"-n")==0) {
	    doNewfunctions=1;
	}
	else if (strcmp(argv[adrPos],"-s")==0) {
	    doSFBandSFC=1;
	}
	else if (strcmp(argv[adrPos],"-2")==0) {
	    useProto=daveProtoMPI2;
	}
	else if (strcmp(argv[adrPos],"-3")==0) {
	    useProto=daveProtoMPI3;
	} 
 	else if (strcmp(argv[adrPos],"-9")==0) {
 	    speed=daveSpeed9k;
 	} 
 	else if (strcmp(argv[adrPos],"-19")==0) {
 	    speed=daveSpeed19k;
 	} 
 	else if (strcmp(argv[adrPos],"-45")==0) {
 	    speed=daveSpeed45k;
 	} 
 	else if (strcmp(argv[adrPos],"-93")==0) {
 	    speed=daveSpeed93k;
 	} 
 	else if (strcmp(argv[adrPos],"-500")==0) {
 	    speed=daveSpeed500k;
 	} 
 	else if (strcmp(argv[adrPos],"-1500")==0) {
 	    speed=daveSpeed1500k;
 	} 
 	
	adrPos++;
	if (argc<=adrPos) {
	    usage();
	    exit(-1);
	}	

    }    
        
    fds.rfd=setPort(argv[adrPos],"38400",'O');
    fds.wfd=fds.rfd;
    
    if (fds.rfd>0) { 
	di =daveNewInterface(fds, "IF1", 0, useProto, daveSpeed187k);
//	di->timeout=500000;
	if (0==daveInitAdapter(di)) {}
	dc =daveNewConnection(di,plcMPI,0,0);  // insert your MPI address here
	if (0==daveConnectPLC(dc)) {
//	    daveDebug=daveDebugAll;
	    printf("ConnectPLC: Success.\n.Now trying the order code.\n");
	    if(useProto!=daveProtoISOTCP243) {
		daveGetOrderCode(dc,buf);
		printf("Order Code: %s\n",buf);	
		wait();
	    }
	    j=daveListBlocks(dc,dbl);
	    for (i=0; i<j; i++) {
		printf("%d blocks of type: %c%c %s\n",
		    dbl[i].count, dbl[i].type[0],dbl[i].type[1],
		    daveBlockName(dbl[i].type[1])
		    );	
	    }	
	    wait();
	    loadBlocksOfType(dc, daveBlockType_OB);
	    loadBlocksOfType(dc, daveBlockType_FB);
	    loadBlocksOfType(dc, daveBlockType_FC);
	    loadBlocksOfType(dc, daveBlockType_DB);
	    loadBlocksOfType(dc, daveBlockType_SDB);
	    if (doSFBandSFC){	    
		loadBlocksOfType(dc, daveBlockType_SFB);
	        loadBlocksOfType(dc, daveBlockType_SFC);
	    }
	    
//	    wait();
	    printf("Now disconnecting\n");	
	    
	    
	    daveDisconnectPLC(dc);
	    daveDisconnectAdapter(di);
	}    
	return 0;
    } else {
	printf("Couldn't open serial port %s\n",argv[adrPos]);	
    	return -1;
    }    
}

/*
    Changes: 
    07/19/04 removed unused vars.
    09/09/04 applied patch for variable Profibus speed from Andrew Rostovtsew.
    09/09/04 removed unused includes byteswap.h, sys/time.h
*/
