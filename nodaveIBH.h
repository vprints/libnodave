/*
 Part of Libnodave, a free communication libray for Siemens S7 300/400 via
 This version uses the IBHLink MPI-Ethernet-Adapter from IBH-Softec.
 www.ibh.de
 
 (C) Thomas Hergenhahn (thomas.hergenhahn@web.de) 2002, 2003.

 Libnodave is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 Libnodave is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Libnodave; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  
*/

#ifndef __nodaveIBH
#define __nodaveIBH
/*
    Define your OS here:
*/    
#include "nodave.h"
/*
    undefine this if you do not want the server functions
*/
#define passiveMode
/* 
    Define the data type for a serial inteface's handle.
*/

#ifdef LINUX
//#include <byteswap.h>
#include <unistd.h>
//#include <sys/time.h>
#include <fcntl.h>
#else    
#ifdef CYGWIN
#include <unistd.h>
#include <fcntl.h>
#else    
#error Fill in what you need for your OS or API.
#endif
#endif

/* 
    This is the packet header used by IBH ethernet NetLink. 
*/
/* 
    another Network MPI header:
*/
typedef struct {
    uc src_conn;
    uc dst_conn;
    uc MPI;
    uc xxx1;
    uc xxx2;
    uc xx22;
    uc len;
    uc func;
    uc packetNumber;
}  MPIheader2;


/*
    Variable read results:
*/    
#define _daveOK 255
#define _daveHardwareError 1
#define _daveNotAcessible 3
#define _daveInvalidAddress 5
#define _daveWrongType 6
#define _daveNotExisting 10

#define daveFuncOpenS7Connection	0xF0
#define daveFuncRead			0x04
#define daveFuncWrite			0x05
/*
    Connect to a PLC via IBH-NetLink. Returns 0 for success and a negative number on errors.
*/
int daveConnectPLC_IBH(daveConnection*dc);

/*
    Disconnect from a PLC via IBH-NetLink. 
    Returns 0 for success and a negative number on errors.
*/
int daveDisconnectPLC_IBH(daveConnection*dc);

/*
    Read len bytes from PLC memory area "area", data block DBnum. 
    Return the Number of bytes read.
    If a buffer pointer is provided, data will be copied into this buffer.
    If it's NULL you can get your data from the dataPointer in daveConnection long
    as you do not send further requests.
*/
int daveReadBytesIBH(daveConnection * dc,int area, int DBnum, int start,int len, void * buffer);
/*
    Write len bytes to PLC memory area "area", data block DBnum. 
*/
int daveWriteBytesIBH(daveConnection * dc,int area, int DBnum, int start,int len, void * buffer);

/*
static inline int daveGetBytesLeft(daveConnection * dc) {
    return ( dc->AnswLen-(int)(dc->dataPointer)+(int)(dc->MPImsg+26));
}
*/

/*
    Read one complete packet. The 3rd byte contains length information.
*/
int _daveReadIBHPacket(daveInterface * di,uc *b);

/*
    Read one complete packet. Byte 3 +4 contain length information.
*/
int _daveReadISOPacket(daveInterface * di,uc *b);

/*
    This is an extended memory compare routine. It can handle don't care flags in the
    sample data.
*/
//int _daveMemcmp(void * a,void *b, size_t len);
/*
    This performs initialization steps with sampled byte sequences. If chal is <>NULL
    it will send this byte sequence.
    It will then wait for a packet and compare it to the sample.
*/
int _daveInitStepIBH(daveInterface * iface, uc * chal, int cl, us* resp,int rl, uc*b);


void _daveSendMPIAck_IBH(daveConnection*dc);

void _daveConstructReadRequest(PDU *p, int area, int DBnum, int start, int bytes);
/*
    build the PDU for a write request
*/
void _daveConstructWriteRequest(PDU *p, int area, int DBnum, int start, int bytes,void * values);
/*
    packet analysis. Return vaslues from analyze.
*/

#define _davePtEmpty -2
#define _davePtMPIAck -3
#define _davePtUnknownMPIFunc -4
#define _davePtUnknownPDUFunc -5
#define _davePtReadResponse 1
#define _davePtWriteResponse 2
/**
    Utilities:
**/
/*
    packet type/function analysis:
*/
int _daveAnalyze(daveConnection * dc);
/*
    send a network level ackknowledge
*/
void _daveSendIBHNetAck(daveConnection * dc);

#ifdef passiveMode

void _daveSendMPIAck2(daveConnection *dc);

#endif

#endif
