/*
 Test and demo program for Libnodave, a free communication libray for Siemens S7.
 This program will read all program and data blocks from your PLC and store them 
 each to a file.
 
 **********************************************************************
 * WARNING: This and other test programs overwrite data in your PLC.  *
 * DO NOT use it on PLC's when anything is connected to their outputs.*
 * This is alpha software. Use entirely on your own risk.             * 
 **********************************************************************
 
 (C) Thomas Hergenhahn (thomas.hergenhahn@web.de) 2002, 2003.

 This is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Libnodave; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "nodave.h"
#include "setport.h"

#ifdef LINUX
#include <unistd.h>
#include <fcntl.h>
#define UNIX_STYLE
#endif

#ifdef CYGWIN
#include <unistd.h>
#include <fcntl.h>
#define UNIX_STYLE
#endif

#ifdef BCCWIN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <fcntl.h>
#include <time.h>
#define WIN_STYLE

    void usage();
    void wait();
#endif


void wait() {
    uc c;
    printf("Press return to continue.\n");
    read(0,&c,1);
}    

void loadBlocksOfType(daveConnection * dc, int blockType) {
    int j, i, uploadID, len, more;
#ifdef UNIX_STYLE
    int fd;
#endif	        
#ifdef WIN_STYLE	    
    HANDLE fd;
    unsigned long res;
#endif	        
    char blockName [20];
    uc blockBuffer[20000],*bb;
    daveBlockEntry dbe[256];   
    j=daveListBlocksOfType(dc, blockType, dbe);
    printf("%d blocks of type %s\n",j,daveBlockName(blockType));
    for (i=0; i<j; i++) {
	printf("%s%d  %d %d\n",
	    daveBlockName(blockType),
	    dbe[i].number, dbe[i].type[0],dbe[i].type[1]);	
	bb=blockBuffer;
	len=0;
	if (0==initUpload(dc, blockType, dbe[i].number, &uploadID)) {
    	    do {
		doUpload(dc,&more,&bb,&len,uploadID);
	    } while (more);
	    sprintf(blockName,"%s%d.mc7",daveBlockName(blockType), dbe[i].number);	
    	    fd=open(blockName,O_RDWR|O_CREAT|O_TRUNC,0644);
    	    write(fd, blockBuffer, len);
    	    close(fd);
    	    endUpload(dc,uploadID);
	}    
    }
}

void usage()
{
    printf("Usage: readout [-d] serial port\n");
    printf("-d will produce a lot of debug messages.\n");
    printf("Example: readout -d /dev/ttyS0\n");
}


int main(int argc, char **argv) {
    int i,j, useProtocol,adrPos;
    daveInterface * di;
    daveConnection * dc;
    daveBlockTypeEntry dbl[20];  // I never saw more then 7 entries
    _daveOSserialType fds;
    adrPos=1;
    useProtocol=daveProtoPPI;
    
    if (argc<2) {
	usage();
	exit(-1);
    }    
    
    while (argv[adrPos][0]=='-') {
	if (strcmp(argv[adrPos],"-d")==0) {
	    daveDebug=daveDebugAll;
	}
	adrPos++;
	if (argc<=adrPos) {
	    usage();
	    exit(-1);
	}	
    }    
        
    fds.rfd=setPort(argv[adrPos],"9600",'E');
    fds.wfd=fds.rfd;
    
    if (fds.rfd>0) { 
	di =daveNewInterface(fds,"IF1",0, useProtocol, daveSpeed187k);
//	if (0==daveInitAdapter(di)) {}
	dc =daveNewConnection(di,2,0,0);  // insert your MPI address here
	
//	if (0==daveConnectPLC(dc)) {
//	    daveDebug=daveDebugAll;
//	    printf("ConnectPLC: Success.\n.Now trying the order code.\n");
	    j=daveListBlocks(dc,dbl);
	    for (i=0; i<j; i++) {
		printf("%d blocks of type: %c%c %s\n",
		    dbl[i].count, dbl[i].type[0],dbl[i].type[1],
		    daveBlockName(dbl[i].type[1])
		    );	
	    }	
	    wait();
	    loadBlocksOfType(dc, daveBlockType_OB);
	    loadBlocksOfType(dc, daveBlockType_FB);
	    loadBlocksOfType(dc, daveBlockType_DB);
	    loadBlocksOfType(dc, daveBlockType_SDB);
/*
    Very strange: loading SFBs does not work when put after loading SFCs
*/	    
	    loadBlocksOfType(dc, daveBlockType_SFB);
	    loadBlocksOfType(dc, daveBlockType_SFC);
	return 0;
    } else {
	printf("Couldn't open serial port %s\n",argv[adrPos]);	
    	return -1;
    }    
}

/*
    Changes: 
    07/19/04 removed unused vars.
    09/09/04 applied patch for variable Profibus speed from Andrew Rostovtsew.
    09/09/04 removed unused includes byteswap.h, sys/time.h
*/
