/*
 Test and demo program for Libnodave, a free communication libray for Siemens S7.
 This program will read all program and data blocks from your PLC and store them 
 each to a file.
 
 **********************************************************************
 * WARNING: This and other test programs overwrite data in your PLC.  *
 * DO NOT use it on PLC's when anything is connected to their outputs.*
 * This is alpha software. Use entirely on your own risk.             * 
 **********************************************************************
 
 (C) Thomas Hergenhahn (thomas.hergenhahn@web.de) 2002, 2003.

 This is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Libnodave; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  
*/
#include <stdlib.h>
#include <stdio.h>
#include "nodave.h"

#include <string.h>
#include <fcntl.h>

#ifdef LINUX
#include "openSocket.h"
#include <unistd.h>
#define UNIX_STYLE
#endif

#ifdef CYGWIN
#include "openSocket.h"
#include <unistd.h>
#define UNIX_STYLE
#endif

#ifdef BCCWIN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "openSocket.h"
#include <time.h>
#define WIN_STYLE
#endif

void wait() {
    uc c;
    printf("Press return to continue.\n");
#ifdef UNIX_STYLE    
    read(0,&c,1);
#endif    
}    

void loadBlocksOfType(daveConnection * dc, int blockType) {
    int j, i, uploadID, len, more, fd;
    char blockName [20];
    uc blockBuffer[20000],*bb;
    daveBlockEntry dbe[256];   
    j=daveListBlocksOfType(dc, blockType, dbe);
    printf("%d blocks of type %s\n",j,daveBlockName(blockType));
    if (j==0) return;
    for (i=0; i<j; i++) {
	printf("%s%d  %d %d\n",
	    daveBlockName(blockType),
	    dbe[i].number, dbe[i].type[0],dbe[i].type[1]);	
	bb=blockBuffer;
	len=0;
	if (0==initUpload(dc, blockType, dbe[i].number, &uploadID)) {
    	    do {
		doUpload(dc,&more,&bb,&len,uploadID);
	    } while (more);
	    sprintf(blockName,"%s%d.mc7",daveBlockName(blockType), dbe[i].number);	
    	    fd=open(blockName,O_RDWR|O_CREAT|O_TRUNC,0644);
    	    write(fd, blockBuffer, len);
    	    close(fd);
    	    endUpload(dc,uploadID);
	}    
    }
}

void usage()
{
    printf("Usage: readoutISO_TCP [-d] [-2] IP-Address of CP\n");
    printf("-2 uses a protocol variant for the CP243. You need to set it, if you use such a CP.\n");
    printf("-d will produce a lot of debug messages.\n");
    printf("--timeout=xxx sets timeout in ms. Useful for slow connections like phone lines.\n");
    printf("Example: readoutISO_TCP -2 192.168.19.1\n");
}


int main(int argc, char **argv) {
    int i,j, useProtocol,adrPos,timeouttime;
    char buf [daveOrderCodeSize];
//    char buf1 [davePartnerListSize];
    daveInterface * di;
    daveConnection * dc;
    daveBlockTypeEntry dbl[20];  // I never saw more then 7 entries
    _daveOSserialType fds;
    adrPos=1;
    useProtocol=daveProtoISOTCP;
    
    if (argc<2) {
	usage();
	exit(-1);
    }    
    
    while (argv[adrPos][0]=='-') {
	if (strcmp(argv[adrPos],"-2")==0) {
	    useProtocol=daveProtoISOTCP243;
	}else
	if (strcmp(argv[adrPos],"-d")==0) {
	    daveDebug=daveDebugAll;
	} else if (strncmp(argv[adrPos],"--timeout=",10)==0) {
	    timeouttime=atol(argv[adrPos]+10);
	    printf("setting timeout to:%d ms\n",timeouttime);
	}
	adrPos++;
	if (argc<=adrPos) {
	    usage();
	    exit(-1);
	}	
    }    
        
    fds.rfd=openSocket(102, argv[adrPos]);
    fds.wfd=fds.rfd;
    
    if (fds.rfd>0) { 
	di =daveNewInterface(fds, "IF1", 0, useProtocol, daveSpeed187k);
	di->timeout=800000000;
	dc =daveNewConnection(di, 2, 0, 2);  // insert your MPI address here
	di->timeout=800000000;
	if (0==daveConnectPLC(dc)) {
//	    daveDebug=daveDebugAll;
//	    printf("ConnectPLC: Success.\n.Now trying the order code.\n");
	    if(useProtocol!=daveProtoISOTCP243) {
		daveGetOrderCode(dc,buf);
		printf("Order Code: %s\n",buf);	
		wait();
	    }
	    j=daveListBlocks(dc,dbl);
	    for (i=0; i<j; i++) {
		printf("%d blocks of type: %c%c %s\n",
		    dbl[i].count, dbl[i].type[0],dbl[i].type[1],
		    daveBlockName(dbl[i].type[1])
		    );	
	    }	
	    wait();
	    loadBlocksOfType(dc, daveBlockType_SFC);
	    loadBlocksOfType(dc, daveBlockType_SFB);
	    loadBlocksOfType(dc, daveBlockType_DB);	    
	    loadBlocksOfType(dc, daveBlockType_OB);
	    loadBlocksOfType(dc, daveBlockType_FC);
	    loadBlocksOfType(dc, daveBlockType_DB);
	    loadBlocksOfType(dc, daveBlockType_SDB);
	    
/*
    Very strange: loading SFBs does not work when put after loading SFCs
*/	    
	    
	    
	}    
	return 0;
    } else {
	printf("Couldn't open TCP port. \nPlease make sure a CP is connected and the IP address is ok. \n");	
    	return -1;
    }    
}

/*
    Changes: 
    00/09/2004  removed unused includes byteswap.h, sys/time.h
*/
