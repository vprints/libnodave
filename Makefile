#
# To build the test programs under Windows with Cygwin, you will have to change a few lines here.
# Just search for the word "Cygwin" to find them.
#
#use this line for compilation under Linux. Comment it out to compile under Windows/Cygwin
CFLAGS=-O0 -Wall -Winline -DLINUX -DHANDMADE_CONVERSIONS
#CFLAGS=-O0 -Wall -Winline
#use this line for compilation under Windows/Cygwin
#CFLAGS=-O0 -Wall -Winline -DCYGWIN -DHANDMADE_CONVERSIONS
PROGRAMS=testISO_TCP readoutISO_TCP testMPI readoutMPI testPPI ibhclient3 \
readoutPPI stopPPI startPPI runStopMPI testPPIload testMPIload ibhtest7 isotest4 \
testMPI2 testISO_TCPload runStopISO_TCP

DYNAMIC_PROGRAMS=testMPId testPPId testISO_TCPd

LIBRARIES=libnodave.so libnodaveIBH.so


#use this line for compilation under Linux. Comment it out to compile under Windows/Cygwin
all: $(PROGRAMS) $(LIBRARIES)
install:
	cp libnodave.so /usr/local/lib
	cp libnodaveIBH.so /usr/local/lib
	ldconfig
dynamic: $(DYNAMIC_PROGRAMS)
#use this line for compilation under Windows/Cygwin
#all: $(PROGRAMS)
nodave.o: nodave.h
testISO_TCP: nodave.o openSocket.o testISO_TCP.o
	gcc nodave.o openSocket.o testISO_TCP.o -o testISO_TCP
testISO_TCPd: nodave.o openSocket.o testISO_TCP.o
	gcc -lnodave testISO_TCP.o -o testISO_TCPd
readoutISO_TCP: nodave.o openSocket.o readoutISO_TCP.o
	gcc nodave.o openSocket.o readoutISO_TCP.o -o readoutISO_TCP
readoutMPI: nodave.o setport.o readoutMPI.o
	gcc nodave.o setport.o readoutMPI.o -o readoutMPI
readoutPPI: nodave.o setport.o readoutPPI.o
	gcc nodave.o setport.o readoutPPI.o -o readoutPPI
testPPIload: nodave.o setport.o testPPIload.o
	gcc nodave.o setport.o testPPIload.o -o testPPIload
testMPI: setport.o testMPI.o nodave.o
	gcc setport.o nodave.o testMPI.o -o testMPI
testMPId: setport.o testMPI.o nodave.o
	gcc -lnodave testMPI.o -o testMPId
testMPIload: nodave.o setport.o testMPIload.o
	gcc nodave.o setport.o testMPIload.o -o testMPIload
testPPI: nodave.o setport.o testPPI.o 
	gcc nodave.o setport.o testPPI.o -o testPPI
testPPId: nodave.o setport.o testPPI.o 
	gcc -lnodave testPPI.o -o testPPId	
profiread: nodave.o setport.o profiread.o 
	gcc nodave.o setport.o profiread.o -o profiread
pr2: nodave.o setport.o pr2.o 
	gcc nodave.o setport.o pr2.o -o pr2
testMPI2: setport.o testMPI2.o nodave.o
	gcc setport.o nodave.o testMPI2.o -o testMPI2
testISO_TCPload: nodave.o openSocket.o testISO_TCPload.o
	gcc nodave.o openSocket.o testISO_TCPload.o -o testISO_TCPload

#testPPIload: nodave.o setport.o testPPIload.o 
#	gcc nodave.o setport.o testPPIload.o -o testPPIload
runStopMPI: nodave.o setport.o runStopMPI.o 
	gcc nodave.o setport.o runStopMPI.o -o runStopMPI
runStopISO_TCP: nodave.o openSocket.o runStopISO_TCP.o
	gcc nodave.o openSocket.o runStopISO_TCP.o -o runStopISO_TCP
stopPPI: nodave.o setport.o stopPPI.o 
	gcc nodave.o setport.o stopPPI.o -o stopPPI
startPPI: nodave.o setport.o startPPI.o 
	gcc nodave.o setport.o startPPI.o -o startPPI


libnodave.so: nodave.o
	ld -shared nodave.o setport.o openSocket.o -o libnodave.so	
libnodaveIBH.so: nodaveIBH.o nodaveIBH.h
	ld -shared nodaveIBH.o nodave.o -o libnodaveIBH.so

ibhclient3: nodaveIBH.o ibhclient3.o nodaveIBH.h openSocket.o
	gcc nodaveIBH.o ibhclient3.o openSocket.o nodave.o -o ibhclient3
ibhtest7: nodaveIBH.o ibhtest7.o nodaveIBH.h openSocket.o
	gcc -lpthread nodaveIBH.o ibhtest7.o openSocket.o nodave.o -o ibhtest7
isotest4: isotest4.o openSocket.o
	gcc -lpthread isotest4.o openSocket.o nodave.o -o isotest4

clean: 
	rm -f $(DYNAMIC_PROGRAMS)
	rm -f $(PROGRAMS)
	rm -f *.o
	rm -f *.so
#use this line for compilation under Windows/Cygwin:
#	rm -f *.exe
