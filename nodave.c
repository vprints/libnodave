/*
 Part of Libnodave, a free communication libray for Siemens S7 200/300/400 via
 the MPI adapter 6ES7 972-0CA22-0XAC
 or  MPI adapter 6ES7 972-0CA23-0XAC
 or  MPI adapter 6ES7 972-0CA33-0XAC
 or  MPI adapter 6ES7 972-0CA11-0XAC,
 IBH-NetLink or CPs 243, 343 and 443
 
 (C) Thomas Hergenhahn (thomas.hergenhahn@web.de) 2002..2004

 Libnodave is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 Libnodave is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Libnodave; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  
*/
#include "nodave.h"
#include <stdio.h>
#include "log.h"
#include <string.h>

/**
    Library specific:
**/
/**
**/
#ifdef LINUX
#define HAVE_UNISTD
#define HAVE_SELECT
#define DECL2
#endif

#ifdef CYGWIN
#define HAVE_UNISTD
#define HAVE_SELECT
#define DECL2
#endif


#ifdef HAVE_UNISTD
#include <unistd.h>
#define daveWriteFile(a,b,c,d) d=write(a,b,c)
#else
#ifdef BCCWIN
//#define DECL2 WINAPI
#define DECL2
#endif
#define daveWriteFile(a,b,c,d) WriteFile(a,b,c, &d, NULL)
#include <winsock2.h>
/* 
void usleep(int x) {
}
*/
void setTimeOut(daveInterface * di, int tmo) {
    COMMTIMEOUTS cto;
    GetCommTimeouts(di->fd.rfd, &cto);
    cto.ReadIntervalTimeout=0;
    cto.ReadTotalTimeoutMultiplier=0;
    cto.ReadTotalTimeoutConstant=tmo/1000;
    SetCommTimeouts(di->fd.rfd,&cto);
}    
#endif
/* 
    This will setup a new interface structure from an initialized
    serial interface's handle and a name.
*/
daveInterface * DECL2 daveNewInterface(_daveOSserialType nfd, char * nname, int localMPI, int protocol, int speed){
    daveInterface * di=(daveInterface *) calloc(1, sizeof(daveInterface));
    if (di) {
	di->name=nname;
	di->fd=nfd;
	di->localMPI=localMPI;
	di->protocol=protocol;
	di->timeout=1000000; /* 0.1 seconds */
	di->nextConnection=0x14;
	di->speed=speed;
	
	di->initAdapter=_daveReturnOkDummy;	
	di->connectPLC=_daveReturnOkDummy;	
	di->disconnectPLC=_daveReturnOkDummy;	
	di->disconnectAdapter=_daveReturnOkDummy;	
	di->listReachablePartners=_daveListReachablePartnersDummy;
	
	switch (protocol) {
	    case daveProtoMPI:
		di->initAdapter=_daveInitAdapterMPI1;
		di->connectPLC=_daveConnectPLCMPI1;
		di->disconnectPLC=_daveDisconnectPLCMPI;
		di->disconnectAdapter=_daveDisconnectAdapterMPI;
		di->exchange=_daveExchangeMPI;
		di->sendMessage=_daveSendMessageMPI;
		di->getResponse=_daveGetResponseMPI;
		di->listReachablePartners=_daveListReachablePartnersMPI;
		break;	
	    	
	    case daveProtoMPI2:
		di->initAdapter=_daveInitAdapterMPI2;
		di->connectPLC=_daveConnectPLCMPI2;
		di->disconnectPLC=_daveDisconnectPLCMPI;
		di->disconnectAdapter=_daveDisconnectAdapterMPI;
		di->exchange=_daveExchangeMPI;
		di->sendMessage=_daveSendMessageMPI;
		di->getResponse=_daveGetResponseMPI;
		di->listReachablePartners=_daveListReachablePartnersMPI;
		di->nextConnection=0x3;
		break;
/*		
	    case daveProtoMPI3:
		di->initAdapter=_daveInitAdapterMPI3;
		di->connectPLC=_daveConnectPLCMPI3;
//		di->disconnectPLC=_daveDisconnectPLCMPI3;
//		di->disconnectAdapter=_daveDisconnectAdapterMPI3;
//		di->exchange=_daveExchangeMPI;
//		di->sendMessage=_daveSendMessageMPI;
//		di->getResponse=_daveGetResponseMPI;
//		di->listReachablePartners=_daveListReachablePartnersMPI3;
		break;	
*/		
	    case daveProtoISOTCP:
	    case daveProtoISOTCP243:
		di->getResponse=_daveGetResponseISO_TCP;
		di->connectPLC=_daveConnectPLCTCP;
		di->exchange=_daveExchangeTCP;
		break;		
		
	    case daveProtoPPI:
		di->getResponse=_daveGetResponsePPI;
		di->exchange=_daveExchangePPI;
		di->timeout=150000; /* 0.15 seconds */
		break;			
	}
#ifdef BCCWIN	
	setTimeOut(di, di->timeout);
#endif
    }
    return di;	
}
/*
    debugging:
    set debug level by setting variable daveDebug. Debugging is split into
    several topics. Output goes to stderr.
    The file descriptor is written after the module name, so you may
    distinguish messages from multiple connections.
    
    naming: all stuff begins with dave... to avoid conflicts with other
    namespaces. Things beginning with _dave.. are not intended for
    public use.
*/

int daveDebug=0;

EXPORTSPEC void DECL2 setDebug(int nDebug) {
    daveDebug=nDebug;
}

/**
    PDU handling:
**/

/*
    set up the header. Needs valid header pointer
*/
void DECL2 _daveInitPDUheader(PDU * p, int type) {
    memset(p->header, 0, sizeof(PDUHeader)/*daveMaxRawLen-40*/); /* attention the PDU begins on different positions! */
    if (type==2 || type==3)
	p->hlen=12;
    else
	p->hlen=10;
    p->param=p->header+p->hlen;
    ((PDUHeader*)p->header)->P=0x32;
    ((PDUHeader*)p->header)->type=type;
    p->dlen=0;
    p->plen=0;
    p->udlen=0;
    p->data=NULL;
    p->udata=NULL;
}

/*
    add parameters after header, adjust pointer to data.
    needs valid header
*/
void DECL2 _daveAddParam(PDU * p,uc * param,us len) {
    p->plen=len;
    memcpy(p->param, param, len);
    ((PDUHeader*)p->header)->plen=bswap_16(len);
    p->data=p->param+len;
    p->dlen=0;
}

/*
    add data after parameters, set dlen
    needs valid header,parameters
*/
void DECL2 _daveAddData(PDU * p,void * data,int len) {
    uc * dn= p->data+p->dlen;
    p->dlen+=len;
    memcpy(dn, data, len);
    ((PDUHeader*)p->header)->dlen=bswap_16(p->dlen);
/*    LOG2("dlen: %d\n", p->dlen); */
}

/*
    add values after value header in data, adjust dlen and data count.
    needs valid header,parameters,data,dlen
*/
void DECL2 _daveAddValue(PDU * p,void * data,int len) {
    us dCount;
//  dCount=* ((us *)(p->data+2));
    uc * dtype;
    dtype=p->data+p->dlen-4+1;			/* position of first byte in the 4 byte sequence */
    dCount=* ((us *)(p->data+p->dlen-4+2));  /* changed for multiple write */
    
    dCount=bswap_16(dCount);
    if (daveDebug & daveDebugPDU)
	LOG2("dCount: %d\n", dCount);
//    if (p->data[1]==4) {	/* bit data, length is in bits */
    if (*dtype==4) {	/* bit data, length is in bits */
	dCount+=8*len;
//    } else if (p->data[1]==9) {	/* byte data, length is in bytes */
    } else if (*dtype==9) {	/* byte data, length is in bytes */
	dCount+=len;
//    } else if (p->data[1]==3) {	/* bit data, length is in bits */
    } else if (* dtype==3) {	/* bit data, length is in bits */
	dCount+=len;	
    } else {
	if (daveDebug & daveDebugPDU)
//	    LOG2("unknown data type/length: %d\n", p->data[1]);
	    LOG2("unknown data type/length: %d\n", *dtype);
    }	    
    if (p->udata==NULL) p->udata=p->data+4;
    p->udlen+=len;	
    if (daveDebug & daveDebugPDU)
	LOG2("dCount: %d\n", dCount);
    dCount=bswap_16(dCount);
//    *((us *)(p->data+2))=dCount;    
    *((us *)(p->data+p->dlen-4+2))=dCount;
    _daveAddData(p, data, len);
}

/*
    add data in user data. Add a user data header, if not yet present.
*/
void DECL2 _daveAddUserData(PDU * p, uc * da, int len) {
    uc udh[]={0xff,9,0,0};
    if (p->dlen==0) {
	if (daveDebug & daveDebugPDU)
	    LOG1("adding user data header.\n");	
	_daveAddData(p, udh, sizeof(udh));
    }
    _daveAddValue(p, da, len);
}

void DECL2 davePrepareReadRequest(daveConnection * dc, PDU *p) {
    uc pa[]=	{daveFuncRead,
		0x00,
		};
    p->header=dc->msgOut+dc->PDUstartO;
    _daveInitPDUheader(p,1);
    _daveAddParam(p, pa, sizeof(pa));
    if (daveDebug & daveDebugPDU) {
	_daveDumpPDU(p);
    }	
}    

void DECL2 davePrepareWriteRequest(daveConnection * dc, PDU *p) {
    uc pa[]=	{daveFuncWrite,
		0x00,
		};
    p->header=dc->msgOut+dc->PDUstartO;		
    _daveInitPDUheader(p,1);
    _daveAddParam(p, pa, sizeof(pa));
    p->dlen=0;
    if (daveDebug & daveDebugPDU) {
	_daveDumpPDU(p);
    }	
}    

void DECL2 daveAddVarToReadRequest(PDU *p, int area, int DBnum, int start, int byteCount) {
    uc pa[]=	{
		0x12, 0x0a, 0x10,0x02,
		0x00,0x1A,	/* insert length in bytes here */
		0x00,0x0B,	/* insert DB number here */
		0x84,		/* change this to real area code */
		0x00,0x00,0xC0	/* insert start address in bits */
		};
    if ((area==daveAnaIn) || (area==daveAnaOut)) {
	pa[3]=4;
	start*=8;			/* bits */
    } else if ((area==daveTimer) || (area==daveCounter)||(area==daveTimer200) || (area==daveCounter200)) {
	pa[3]=area;
    } else {
	start*=8;			/* bits */
    }
    pa[4]=byteCount / 256;		
    pa[5]=byteCount & 0xff;		
    pa[6]=DBnum / 256;		
    pa[7]=DBnum & 0xff;		
    pa[8]=area;		
    pa[11]=start & 0xff;
    pa[10]=(start / 0x100) & 0xff;
    pa[9]=start / 0x10000; 
    
    p->param[1]++;
    memcpy(p->param+p->plen, pa, sizeof(pa));
    p->plen+=sizeof(pa);
    ((PDUHeader*)p->header)->plen=bswap_16(p->plen);
    p->data=p->param+p->plen;
    p->dlen=0;
    if (daveDebug & daveDebugPDU) {
	_daveDumpPDU(p);
    }	
}    

void DECL2 daveAddBitVarToReadRequest(PDU *p, int area, int DBnum, int start, int byteCount) {
    uc pa[]=	{
		0x12, 0x0a, 0x10,
		0x01,		/* single bits */
		0x00,0x1A,	/* insert length in bytes here */
		0x00,0x0B,	/* insert DB number here */
		0x84,		/* change this to real area code */
		0x00,0x00,0xC0	/* insert start address in bits */
		};
    pa[4]=byteCount / 256;		
    pa[5]=byteCount & 0xff;		
    pa[6]=DBnum / 256;		
    pa[7]=DBnum & 0xff;		
    pa[8]=area;		
/*    start*=8; */	/* bits */
    pa[11]=start & 0xff;
    pa[10]=(start / 0x100) & 0xff;
    pa[9]=start / 0x10000; 
    
    p->param[1]++;
    memcpy(p->param+p->plen, pa, sizeof(pa));
    p->plen+=sizeof(pa);
    ((PDUHeader*)p->header)->plen=bswap_16(p->plen);
    p->data=p->param+p->plen;
    p->dlen=0;
    if (daveDebug & daveDebugPDU) {
	_daveDumpPDU(p);
    }
}    

void DECL2 daveAddVarToWriteRequest(PDU *p, int area, int DBnum, int start, int byteCount, void * buffer) {
    uc saveData[1024];
    uc da[]=	{0,4,0,0,};
    uc pa[]=	{
		0x12, 0x0a, 0x10,0x02,
		0x00,0x1A,	/* insert length in bytes here */
		0x00,0x0B,	/* insert DB number here */
		0x84,		/* change this to real area code */
		0x00,0x00,0xC0	/* insert start address in bits */
		};
    if ((area==daveTimer) || (area==daveCounter)||(area==daveTimer200) || (area==daveCounter200)) {    
	pa[3]=area;
	pa[4]=((byteCount+1)/2) / 0x100;
	pa[5]=((byteCount+1)/2) & 0xff;
    } else if ((area==daveAnaIn) || (area==daveAnaOut)) {
	pa[3]=4;
	pa[4]=((byteCount+1)/2) / 0x100;
	pa[5]=((byteCount+1)/2) & 0xff;
    } else {	
	pa[4]=byteCount / 0x100;		
	pa[5]=byteCount & 0xff;	
		
    }
    pa[6]=DBnum / 256;		
    pa[7]=DBnum & 0xff;		
    pa[8]=area;		
    start*=8;			/* number of bits */
    pa[11]=start & 0xff;
    pa[10]=(start / 0x100) & 0xff;
    pa[9]=start / 0x10000; 
    p->param[1]++;
/*    
    if(p->dlen%2) {
	_daveAddData(p, da, 1); / * add a null byte to fill up last data to word boundary * /
	LOG1("****************adding a fill byte\n");
    }
*/    
    if(p->dlen){
	 memcpy(saveData, p->data, p->dlen);
//	 memcpy(p->data+sizeof(pa), p->data, p->dlen);
	 memcpy(p->data+sizeof(pa), saveData, p->dlen);
    }	 
    memcpy(p->param+p->plen, pa, sizeof(pa));
    p->plen+=sizeof(pa);
    
    ((PDUHeader*)p->header)->plen=bswap_16(p->plen);
    p->data=p->param+p->plen;
/*    
    if(p->dlen%2) {
	_daveAddData(p, da, 1); / * add a null byte to fill up last data to word boundary * /
	LOG1("adding a fill byte\n");
    }	
*/    
    _daveAddData(p, da, sizeof(da));
    _daveAddValue(p, buffer, byteCount);
//    if(p->dlen%2) {
    if(byteCount%2) {
	_daveAddData(p, da, 1); /* add a null byte to fill up last data to word boundary */
//	LOG1("*************************adding a fill byte\n");
    }    
    if (daveDebug & daveDebugPDU) {
	_daveDumpPDU(p);
    }	
}    

void DECL2 daveAddBitVarToWriteRequest(PDU *p, int area, int DBnum, int start, int byteCount, void * buffer) {
    uc saveData[1024];
    uc da[]=	{0,3,0,0,};
    uc pa[]=	{
		0x12, 0x0a, 0x10,
		0x01,		/* single bits */
		0x00,0x1A,	/* insert length in bytes here */
		0x00,0x0B,	/* insert DB number here */
		0x84,		/* change this to real area code */
		0x00,0x00,0xC0	/* insert start address in bits */
		};
    if ((area==daveTimer) || (area==daveCounter)||(area==daveTimer200) || (area==daveCounter200)) {    
	pa[3]=area;
	pa[4]=((byteCount+1)/2) / 0x100;
	pa[5]=((byteCount+1)/2) & 0xff;
    } else if ((area==daveAnaIn) || (area==daveAnaOut)) {
	pa[3]=4;
	pa[4]=((byteCount+1)/2) / 0x100;
	pa[5]=((byteCount+1)/2) & 0xff;
    } else {	
	pa[4]=byteCount / 0x100;		
	pa[5]=byteCount & 0xff;			
    }
    pa[6]=DBnum / 256;		
    pa[7]=DBnum & 0xff;		
    pa[8]=area;		
    pa[11]=start & 0xff;
    pa[10]=(start / 0x100) & 0xff;
    pa[9]=start / 0x10000; 
    p->param[1]++;
/*    
    if(p->dlen%2) {
	_daveAddData(p, da, 1); / * add a null byte to fill up last data to word boundary * /
	LOG1("****************adding a fill byte\n");
    }
*/    
    if(p->dlen){
	 memcpy(saveData, p->data, p->dlen);
//	 memcpy(p->data+sizeof(pa), p->data, p->dlen);
	 memcpy(p->data+sizeof(pa), saveData, p->dlen);
    }	 
    memcpy(p->param+p->plen, pa, sizeof(pa));
    p->plen+=sizeof(pa);
    
    ((PDUHeader*)p->header)->plen=bswap_16(p->plen);
    p->data=p->param+p->plen;
/*    
    if(p->dlen%2) {
	_daveAddData(p, da, 1); / * add a null byte to fill up last data to word boundary * /
	LOG1("adding a fill byte\n");
    }	
*/    
    _daveAddData(p, da, sizeof(da));
    _daveAddValue(p, buffer, byteCount);
//    if(p->dlen%2) {
    if(byteCount%2) {
	_daveAddData(p, da, 1); /* add a null byte to fill up last data to word boundary */
	LOG1("*************************adding a fill byte\n");
    }    
    if (daveDebug & daveDebugPDU) {
	_daveDumpPDU(p);
    }	
}    


/*
    build the PDU for a bit write request    
*/
void DECL2 _daveConstructBitWriteRequest(PDU *p, int area, int DBnum, int start, int bytes,void * values) {
    uc pa[]=	{daveFuncWrite,
		0x01,
		0x12, 0x0a, 0x10,
		0x01,		/* bits! */
		0x00,0x1A,	/* insert length in bytes here */
		0x00,0x0B,	/* insert DB number here */
		0x84,		/* change this to real area code */
		0x00,0x00,0xC0	/* insert start address in bits */
		};
    uc da[]=	{0,3,0,0,};
    pa[6]=bytes / 0x100;		
    pa[7]=bytes & 0xff;		
    pa[8]=DBnum / 0x100;		
    pa[9]=DBnum & 0xff;		
    pa[10]=area;		
/*    start*=8;			*/ /* bits */
    pa[13]=start & 0xff;
    pa[12]=(start / 0x100) & 0xff;
    pa[11]=start / 0x10000; 
    _daveInitPDUheader(p,1);
    _daveAddParam(p, pa, sizeof(pa));
    _daveAddData(p, da, sizeof(da));
    _daveAddValue(p, values, bytes);
    if (daveDebug & daveDebugPDU) {
	_daveDumpPDU(p);
    }
}    

/*
    Get the eror code from a PDU, if one.
*/
int DECL2 daveGetPDUerror(PDU * p) {
    if (p->header[1]==2 || p->header[1]==3) {
	return daveGetU16from(p->header+10);
    } else
	return 0;
}

/*
    Sets up pointers to the fields of a received message. 
*/
int DECL2 _daveSetupReceivedPDU(daveConnection * dc, PDU * p) {
	int res = daveResCannotEvaluatePDU;    
	p->header=dc->msgIn+dc->PDUstartI;
	
        if (p->header[1]==2 || p->header[1]==3) {
	    p->hlen=12;
	} else
	    p->hlen=10;
	p->param=p->header+p->hlen;    
	if (daveDebug & daveDebugPDU)
	    _daveDump("PDU header", p->header, p->hlen);
	p->plen=256*p->header[6] + p->header[7];
	p->data=p->param+p->plen;
        p->dlen=256*p->header[8] + p->header[9];
	p->udlen=0; 
	p->udata=NULL;
	if ((p->param[0]==daveFuncRead)|| (p->param[0]==0)) {
	    if ((p->data[0]==255)&&(p->dlen>4)) {
		res=daveResOK;
		p->udata=p->data+4;
		p->udlen=p->data[2]*0x100+p->data[3];
		if (p->data[1]==4) {
    		    p->udlen>>=3;	/* len is in bits, adjust */
		} else if (p->data[1]==9) {
		    /* len is already in bytes, ok */
		} else if (p->data[1]==3) {
		    /* len is in bits, but there is a byte per result bit, ok */
		} else {
		    if (daveDebug & daveDebugPDU)
			LOG2("fixme: what to do with data type %d?\n",p->data[1]);
		}	    
	    } else {
		if ((p->data[0]==0x0A)) {
		    res=daveResItemNotAvailable;    
		}
		if ((p->data[0]==0x05)) {
		    res=daveAddressOutOfRange;
		}
		if ((p->data[0]==0x06)) {
		    res=daveResMultipleBitsNotSupported;
		}
		if ((p->data[0]==0x03)) {
		    res=daveResItemNotAvailable200;
		}
	    }
	} else if (p->param[0]==daveFuncWrite) {
	    if ((p->data[0]==255)) {
		res=daveResOK;
	    } else if ((p->data[0]==0x0A)) {
		res=daveResItemNotAvailable;    
	    } else if ((p->data[0]==0x06)) {
	        res=daveResMultipleBitsNotSupported;
	    } else if ((p->data[0]==0x03)) {
	        res=daveResItemNotAvailable200;
	    } else if ((p->data[0]==0x07)) {
	        res=daveWriteDataSizeMismatch;
	    }
	}
	if (daveDebug & daveDebugPDU) {
	    _daveDumpPDU(p);
	}
	return res;
}

/*****
    Utilities:
****/
/*
    This is an extended memory compare routine. It can handle don't care and stop flags 
    in the sample data. A stop flag lets it return success.
*/
int DECL2 _daveMemcmp(us * a, uc *b, size_t len) {
    unsigned int i;
    us * a1=(us*)a;
    uc * b1=(uc*)b;
    for (i=0;i<len;i++){
	if (((*a1)&0xff)!=*b1) {
	    if (((*a1)&0x100)!=0x100) return 1;
	    if (((*a1)&0x200)!=0x200) return 0;
	}
	a1++;
	b1++;
    }
    return 0;
}

/*
    Hex dump:
*/
void DECL2 _daveDump(char * name,uc*b,int len) {
    int j;
    LOG2("%s: ",name);
    if (len>daveMaxRawLen) len=daveMaxRawLen; 	/* this will avoid to dump zillions of chars */
    for (j=0; j<len; j++){
	LOG2("%02X,",b[j]);
    }
    LOG1("\n");
}

void DECL2 _daveDumpPDUError(PDU * p) {
    LOG1("PDU result: ");
    if (p->header[10]==0) LOG1("ok\n");
    else {
	LOG1("error: ");
	if (p->header[10]==0xd2) {
		if (p->header[11]==1) LOG1("block name syntax error."); else
		if (p->header[11]==9) LOG1("block not present."); else
		if (p->header[11]==16) LOG1("block number too large."); else
		LOG1("unknown error!! ");
	} 
	LOG1("\n");
    }	
}

/*
    Hex dump PDU:
*/
void DECL2 _daveDumpPDU(PDU * p) {
    int i,dl;
    uc * pd;
    _daveDump("PDU header", p->header, p->hlen);
    LOG3("plen: %d dlen: %d\n",p->plen, p->dlen);
    if(p->plen>0) _daveDump("Parameter",p->param,p->plen);
    if ((p->plen==2)&&(p->param[0]==daveFuncRead)) {
	pd=p->data;
	for (i=0;i<p->param[1];i++) {
	    _daveDump("Data hdr ",pd,4);
	    
	    dl=0x100*pd[2]+pd[3];
	    if (pd[1]==4) dl/=8;
	    pd+=4;        
	    _daveDump("Data     ",pd,dl);
	    if(i<p->param[1]-1) dl=dl+(dl%2);  	// the PLC places extra bytes at the end of all 
						// but last result, if length is not a multiple 
						// of 2
	    pd+=dl;
	}
    } else if ((p->header[1]==1)&&/*(p->plen==2)&&*/(p->param[0]==daveFuncWrite)) {
	pd=p->data;
	for (i=0;i<p->param[1];i++) {
	    _daveDump("Write Data hdr ",pd,4);
	    
	    dl=0x100*pd[2]+pd[3];
	    if (pd[1]==4) dl/=8;
	    pd+=4;        
	    _daveDump("Data     ",pd,dl);
	    if(i<p->param[1]-1) dl=dl+(dl%2);  	// the PLC places extra bytes at the end of all 
						// but last result, if length is not a multiple 
						// of 2
	    pd+=dl;
	}
    } else {    
	if(p->dlen>0) {
	    if(p->udlen==0)
		_daveDump("Data     ",p->data,p->dlen);
	    else
		_daveDump("Data hdr ",p->data,4);
	}	
	if(p->udlen>0) _daveDump("result Data ",p->udata,p->udlen);
    }
    if ((p->header[1]==2)||(p->header[1]==3)) _daveDumpPDUError(p);
}

/*
    name Objects:
*/
char * DECL2 daveBlockName(uc bn) {
    switch(bn) {
	case daveBlockType_OB: return "OB";
	case daveBlockType_DB: return "DB";
	case daveBlockType_SDB: return "SDB";
	case daveBlockType_FC: return "FC";
	case daveBlockType_SFC: return "SFC";
	case daveBlockType_FB: return "FB";
	case daveBlockType_SFB: return "SFB";
	default:return "unknown block type!";
    }    
}

char * DECL2 daveAreaName(uc n) {
    switch (n) {
	case daveDB:		return "DB";
	case daveInputs:	return "Inputs";
	case daveOutputs:	return "Outputs";
	case daveFlags:		return "Flags";
	default:return "unknown area!";
    }	
}
/*
    Functions to load blocks from PLC:
*/
void DECL2 _daveConstructUpload(PDU *p,char blockType, int blockNr) {
    uc pa[]=	{0x1d,
    0,0,0,0,0,0,0,9,0x5f,0x30,0x41,48,48,48,48,49,65};
    pa[11]=blockType;
    sprintf((char*)(pa+12),"%05d",blockNr);
    pa[17]='A';
    _daveInitPDUheader(p,1);
    _daveAddParam(p, pa, sizeof(pa));
    if (daveDebug & daveDebugPDU) {
	_daveDumpPDU(p);
    }	
}

void DECL2 _daveConstructDoUpload(PDU * p, int uploadID) {
    uc pa[]=	{0x1e,0,0,0,0,0,0,1};
    pa[7]=uploadID;
    _daveInitPDUheader(p,1);
    _daveAddParam(p, pa, sizeof(pa));
    if (daveDebug & daveDebugPDU) {
	_daveDumpPDU(p);
    }	
}    

void DECL2 _daveConstructEndUpload(PDU * p, int uploadID) {
    uc pa[]=	{0x1f,0,0,0,0,0,0,1};
    pa[7]=uploadID;
    _daveInitPDUheader(p,1);
    _daveAddParam(p, pa, sizeof(pa));
    if (daveDebug & daveDebugPDU) {
	_daveDumpPDU(p);
    }	
}    

int DECL2 daveBuildPDU(daveConnection * dc, PDU*p2,uc *pa,int psize, uc *ud,int usize) {
    int res;
    PDU p;
    p.header=dc->msgOut+dc->PDUstartO;
    _daveInitPDUheader(&p, 7);
    _daveAddParam(&p, pa, psize);
    _daveAddUserData(&p, ud, usize);
    if (daveDebug & daveDebugPDU) {
	_daveDumpPDU(&p);
    }	
    res=_daveExchange(dc, &p);
    if (res==daveResOK) {
	res=_daveSetupReceivedPDU(dc,p2);
	if (daveDebug & daveDebugPDU) {
	    _daveDumpPDU(p2);
	}
    }	
    return res;
}    	


int DECL2 daveListBlocksOfType(daveConnection * dc,uc type,daveBlockEntry * buf) {
    int res,i, len;
    PDU p2;
    uc * buffer=(uc*)buf;
//Parameter: 00,01,12,04,11,43,01,00,
//Data     : 0A,00,00,00,

    uc pa[]={0,1,18,4,17,67,2,0};
    uc da[]={'0','0'};
    uc pam[]={0,1,18,8,0x12,0x43,2,1,0,0,0,0};
    uc dam[]={0,0x0a,0,0};
    da[1]=type;
    res=daveBuildPDU(dc, &p2,pa, sizeof(pa), da, sizeof(da));
/*    
    res=p2.udlen/sizeof(daveBlockEntry);
    if (buf) {
	memcpy(buf, p2.udata, p2.udlen);
	for (i=0; i<res; i++) {
	    buf[i].number=bswap_16(buf[i].number);
	}
    }	
    return res;
*/    
    
//    printf("here we are\n");
    len=0;
    while (p2.param[9]!=0) {
	if (buffer!=NULL) memcpy(buffer+len,p2.udata,p2.udlen);
        dc->resultPointer=p2.udata;
        dc->_resultPointer=p2.udata;
        len+=p2.udlen;
//	printf("more data\n");
	res=daveBuildPDU(dc, &p2,pam, sizeof(pam), dam, sizeof(dam));
    }
    
    
    if (res==daveResOK) {
	if (buffer!=NULL) memcpy(buffer+len,p2.udata,p2.udlen);
//        if (buffer!=NULL) memcpy(buffer,p2.udata,p2.udlen);
        dc->resultPointer=p2.udata;
        dc->_resultPointer=p2.udata;
        len+=p2.udlen;
    } else {
	LOG2("daveListBlocksOfType: %d\n",res);
    }
    dc->AnswLen=len;
    res=len/sizeof(daveBlockEntry);
    for (i=0; i<res; i++) {
        buf[i].number=bswap_16(buf[i].number);
    }
    return res;
    
}    
/*
    doesn't work on S7-200
*/
int DECL2 daveGetOrderCode(daveConnection * dc,char * buf) {
    int res=0;
    PDU p2;
    uc pa[]={0,1,18,4,17,68,1,0};
    uc da[]={1,17,0,1};  /* SZL-ID 0x111 index 1 */
    daveBuildPDU(dc, &p2,pa, sizeof(pa), da, sizeof(da));
    if (buf) {
	memcpy(buf, p2.udata+10, daveOrderCodeSize);
	buf[daveOrderCodeSize]=0;
    }	
    return res;
}

int DECL2 daveReadSZL(daveConnection * dc, int ID, int index, void * buffer) {
    int res,len;
    PDU p2;
    uc pa[]={0,1,18,4,0x11,0x44,1,0};
    uc da[]={1,17,0,1};  /* SZL-ID 0x111 index 1 */
    
    uc pam[]={0,1,18,8,0x12,0x44,1,1,0,0,0,0};
    uc dam[]={0,0x0a,0,0};
    da[0]=ID / 0x100;
    da[1]=ID % 0x100;
    da[2]=index / 0x100;
    da[3]=index % 0x100;
    res=daveBuildPDU(dc, &p2,pa, sizeof(pa), da, sizeof(da));
    
//    printf("here we are\n");
    len=0;
    while (p2.param[9]!=0) {
	if (buffer!=NULL) memcpy((uc *)buffer+len,p2.udata,p2.udlen);
        dc->resultPointer=p2.udata;
        dc->_resultPointer=p2.udata;
        len+=p2.udlen;
//	printf("more data\n");
	res=daveBuildPDU(dc, &p2,pam, sizeof(pam), dam, sizeof(dam));
    }
    
    
    if (res==daveResOK) {
	if (buffer!=NULL) memcpy((uc *)buffer+len,p2.udata,p2.udlen);
//        if (buffer!=NULL) memcpy(buffer,p2.udata,p2.udlen);
        dc->resultPointer=p2.udata;
        dc->_resultPointer=p2.udata;
        len+=p2.udlen;
    } 
    dc->AnswLen=len;
    return res;
}

int DECL2 daveGetBlockInfo(daveConnection * dc, daveBlockInfo *dbi, uc type, int number)
{
    int res;
    uc pa[]={0,1,18,4,17,67,3,0};	   /* param */
    uc da[]={'0',0,'0','0','0','1','0','A'};
    PDU p2;
    sprintf((char*)(da+2),"%05d",number);
    da[1]=type;
    da[7]='A';
    daveBuildPDU(dc, &p2,pa, sizeof(pa), da, sizeof(da));    
    if (dbi && p2.udlen==sizeof(daveBlockInfo)) {
	memcpy(dbi, p2.udata, p2.udlen);
	dbi->number=bswap_16(dbi->number);
        dbi->length=bswap_16(dbi->length);
    }
    return res;	
}

int DECL2 daveListBlocks(daveConnection * dc,daveBlockTypeEntry * buf) {
    int res,i;
    PDU p2;
//Parameter: 00,01,12,04,11,43,01,00,
//Data     : 0A,00,00,00,
    
    uc pa[]={0,1,18,4,17,67,1,0};
    uc da[]={10,0,0,0}; 
    daveBuildPDU(dc, &p2,pa, sizeof(pa), da, sizeof(da));    
    res=p2.udlen/sizeof(daveBlockTypeEntry);
    if (buf) {
	memcpy(buf, p2.udata, p2.udlen);
	for (i=0; i<res; i++) {
	    buf[i].count=bswap_16(buf[i].count);
	}	
    }	
    return res;
}

/*
    Read len bytes from PLC memory area "area", data block DBnum. 
    Return the Number of bytes read.
    If a buffer pointer is provided, data will be copied into this buffer.
    If it's NULL you can get your data from the resultPointer in daveConnection long
    as you do not send further requests.
*/
int DECL2 daveReadBytes(daveConnection * dc,int area, int DBnum, int start,int len, void * buffer){
    PDU p1,p2;
    int errorState,res;
    
    p1.header=dc->msgOut+dc->PDUstartO;
    davePrepareReadRequest(dc, &p1);
    daveAddVarToReadRequest(&p1, area, DBnum, start, len);
    errorState=_daveExchange(dc, &p1);
    if (errorState) return errorState;
    res=_daveSetupReceivedPDU(dc, &p2);
    if (daveDebug & daveDebugPDU)
	LOG3("_daveSetupReceivedPDU() returned: %d=%s\n", res,daveStrerror(res));
    if (res!=0) {
	dc->resultPointer=NULL;
	dc->_resultPointer=NULL;
	dc->AnswLen=0;
	return res;
    }
    if (p2.udlen==0) {
	dc->resultPointer=NULL;
	dc->_resultPointer=NULL;
	dc->AnswLen=0;
	return daveResCPUNoData; 
    }	
    if (p2.param[0]==daveFuncRead) {
/*	if (p2.udata==NULL) 
//	    errorState+=p2.data[0]; 
//	else {
*/
	    if (buffer!=NULL) memcpy(buffer,p2.udata,p2.udlen);
	    dc->resultPointer=p2.udata;
	    dc->_resultPointer=p2.udata;
	    dc->AnswLen=p2.udlen;
/*	}    */
    } else {
	errorState|=2048;
    }
    return errorState;
}


/*
    Read len BITS from PLC memory area "area", data block DBnum. 
    Return the Number of bytes read.
    If a buffer pointer is provided, data will be copied into this buffer.
    If it's NULL you can get your data from the resultPointer in daveConnection long
    as you do not send further requests.
*/
int DECL2 daveReadBits(daveConnection * dc,int area, int DBnum, int start,int len, void * buffer){
    PDU p1,p2;
    int errorState,res;
    
    p1.header=dc->msgOut+dc->PDUstartO;
    davePrepareReadRequest(dc, &p1);
    daveAddBitVarToReadRequest(&p1, area, DBnum, start, len);

    
    errorState=_daveExchange(dc, &p1);
    res=_daveSetupReceivedPDU(dc, &p2);
    if (daveDebug & daveDebugPDU)
	LOG3("_daveSetupReceivedPDU() returned: %d=%s\n", res,daveStrerror(res));
    if (res!=0) {
	dc->resultPointer=NULL;
	dc->_resultPointer=NULL;
	dc->AnswLen=0;
	return res;
    }
    if (p2.udlen==0) {
	dc->resultPointer=NULL;
	dc->_resultPointer=NULL;
	dc->AnswLen=0;
	return daveResCPUNoData; 
    }	
    if (p2.param[0]==daveFuncRead) {
	    if (buffer!=NULL) memcpy(buffer,p2.udata,p2.udlen);
	    dc->resultPointer=p2.udata;
	    dc->_resultPointer=p2.udata;
	    dc->AnswLen=p2.udlen;
    } else {
	errorState|=2048;
    }
    return errorState;
}


/*
    Execute a predefined read request. Store results into the resultSet structure.
*/
int DECL2 daveExecReadRequest(daveConnection * dc, PDU *p, daveResultSet* rl){
    PDU p2;
    uc * q;
    daveResult * cr, *c2;
    int errorState, i, len, rlen;
    errorState=_daveExchange(dc, p);
    _daveSetupReceivedPDU(dc, &p2);
    if (p2.udlen==0) {
	dc->resultPointer=NULL;
	dc->_resultPointer=NULL;
	dc->AnswLen=0;
	return daveResCPUNoData; 
    }	
    if (p2.param[0]==daveFuncRead) {
/*	if (p2.udata==NULL) 
//	    errorState+=p2.data[0]; 
//	else {
//	    if (buffer!=NULL) memcpy(buffer,p2.udata,p2.udlen);
//	    dc->numResults=p2.param[1];
//	    dc->resultPointer=p2.udata;
//	    dc->_resultPointer=p2.udata;
//	    dc->AnswLen=p2.udlen;
*/
	    i=0;
	    if (rl!=NULL) {
		cr=calloc(p2.param[1], sizeof(daveResult));
		rl->numResults=p2.param[1];
		rl->results=cr;
		c2=cr;
		q=p2.data;
		rlen=p2.dlen;
		while (i<p2.param[1]) {
/*		    printf("result %d: %d  %d %d %d\n",i, *q,q[1],q[2],q[3]); */
		    if ((*q==255)&&(rlen>4)) {
			len=q[2]*0x100+q[3];
			if (q[1]==4) {
    			    len>>=3;	/* len is in bits, adjust */
			} else if (q[1]==9) {
			    /* len is already in bytes, ok */
			} else if (q[1]==3) {
			    /* len is in bits, but there is a byte per result bit, ok */
			} else {
			    if (daveDebug & daveDebugPDU)
				LOG2("fixme: what to do with data type %d?\n",q[1]);
			}
		    } else {
			len=0;
		    }	
/*		    printf("Store result %d length:%d\n", i, len); */
		    c2->length=len;
		    if(len>0){
			 c2->bytes=malloc(len);
			 memcpy(c2->bytes, q+4, len);
		    }	 
		    c2->error=daveUnknownError;
		    if (q[0]==0x0A) {	/* 300 and 400 families */
			c2->error=daveResItemNotAvailable;    
		    } else
		    if (q[0]==0x03) {	/* 200 family */
			c2->error=daveResItemNotAvailable;    
		    } else
		    if (q[0]==0x05) {
			c2->error=daveAddressOutOfRange;    
		    } else
		    if (q[0]==0xFF) {
			c2->error=daveResOK;    
		    }
/*		    printf("Error %d\n", c2->error); */
		    q+=len+4;
		    rlen-=len;
		    if ((len % 2)==1) {
		        q++;
		        rlen--;
		    } 
		    c2++;
	    	    i++;
		}
	    } 
	    
/*	}    */
    } else {
	errorState|=2048;
    }
    return errorState;
}

/*
    Execute a predefined write request.
*/
int DECL2 daveExecWriteRequest(daveConnection * dc, PDU *p, daveResultSet* rl){
    PDU p2;
    uc * q;
    daveResult * cr, *c2;
    int errorState, i, rlen;
    errorState=_daveExchange(dc, p);
    _daveSetupReceivedPDU(dc, &p2);
    if (p2.param[0]==daveFuncWrite) {
        if (rl!=NULL) {
	    cr=calloc(p2.param[1], sizeof(daveResult));
	    rl->numResults=p2.param[1];
	    rl->results=cr;
	    c2=cr;
	    q=p2.data;
	    rlen=p2.dlen;
	    i=0;
	    while (i<p2.param[1]) {
/*		printf("result %d: %d  %d %d %d\n",i, *q,q[1],q[2],q[3]); */
		c2->error=daveUnknownError;
		if (q[0]==0x0A) {	/* 300 and 400 families */
		    c2->error=daveResItemNotAvailable;    
		} else if (q[0]==0x03) {	/* 200 family */
		    c2->error=daveResItemNotAvailable;    
		} else if (q[0]==0x05) {
		    c2->error=daveAddressOutOfRange;    
		} else if (q[0]==0xFF) {
		    c2->error=daveResOK;   
		} else if (q[0]==0x07) {
	    	    c2->error=daveWriteDataSizeMismatch;
		}	
/*		    printf("Error %d\n", c2->error); */
		q++;
		c2++;
		i++;
	    } 
	} else {
	    errorState|=2048;
	}    
    }
    return errorState;
}


int DECL2 daveUseResult(daveConnection * dc, daveResultSet rl, int n){
    daveResult * dr;
    if (rl.numResults==0) return daveEmptyResultSetError;
    if (n>=rl.numResults) return daveEmptyResultSetError;
    dr = &(rl.results[n]);
    if (dr->error!=0) return dr->error;
    if (dr->length<=0) return daveEmptyResultError;
    
    dc->resultPointer=dr->bytes;
    dc->_resultPointer=dr->bytes;
    return 0;
}

void DECL2 daveFreeResults(daveResultSet * rl){
    daveResult * r;
    int i;
/*    printf("result set: %p\n",rl); */
    for (i=0; i<rl->numResults; i++) {
        r=&(rl->results[i]);
/*	printf("result: %p bytes at:%p\n",r,r->bytes); */
	if (r->bytes!=NULL) free(r->bytes);
    }
/*    free(rl);	*/ /* This is NOT malloc'd by library but in the application's memory space! */
}


/* 
    This will setup a new connection structure using an initialized
    daveInterface and PLC's MPI address.
*/
daveConnection * DECL2 daveNewConnection(daveInterface * di, int MPI, int rack, int slot) {
    daveConnection * dc=(daveConnection *) calloc(1,sizeof(daveConnection));
    if (dc) {
	dc->iface=di;
	dc->MPIAdr=MPI;

	dc->templ.src_conn=20;	/* for MPI over IBHLink */
	dc->templ.MPI=MPI;
	dc->templ.localMPI=di->localMPI; //????
	dc->rack=rack;
	dc->slot=slot;
	
	dc->maxPDUlength=240;
	dc->connectionNumber=di->nextConnection;	// 1/10/05 trying Andrew's patch
//	di->nextConnection++;				// 1/10/05 trying Andrew's patch
	
	switch (di->protocol) {
	    case daveProtoMPI:		/* my first Version of MPI */
		dc->PDUstartO=8;	/* position of PDU in outgoing messages */
		dc->PDUstartI=8;	/* position of PDU in incoming messages */
		di->ackPos=6;		/* position of 0xB0P in ack packet */
		break;
	    case daveProtoMPI2:		/* Andrew's Version of MPI */
		dc->PDUstartO=6;	/* position of PDU in outgoing messages */
		dc->PDUstartI=6;	/* position of PDU in incoming messages */
		di->ackPos=4;		/* position of 0xB0P in ack packet */
		break;	
/*		
	    case daveProtoMPI3:		/ * Step 7 Version of MPI * /
		dc->PDUstartO=12;	/ * position of PDU in outgoing messages * /
		dc->PDUstartI=12;	/ * position of PDU in incoming messages * /
		di->ackPos=10;		/ * position of 0xB0P in ack packet * /
		break;	
*/	
	    case daveProtoPPI:
		dc->PDUstartO=3;	/* position of PDU in outgoing messages */
		dc->PDUstartI=7;	/* position of PDU in incoming messages */
		break;	
		
	    case daveProtoISOTCP:
	    case daveProtoISOTCP243:
		dc->PDUstartO=7;	/* position of PDU in outgoing messages */
		dc->PDUstartI=7;	/* position of PDU in incoming messages */
		di->timeout=1500000;
		break;	
	    case daveProtoIBH:	
		dc->PDUstartI= sizeof(IBHpacket)+sizeof(MPIheader);	
		break;
	    default:
		dc->PDUstartO=8;	/* position of PDU in outgoing messages */
		dc->PDUstartI=8;	/* position of PDU in incoming messages */
		fprintf(stderr,"Unknown protocol on interface %s\n",di->name);	
	}    
#ifdef BCCWIN	
	setTimeOut(di, di->timeout);
#endif
    }
    return dc;	
}

int DECL2 daveWriteBytes(daveConnection * dc,int area, int DB, int start, int len, void * buffer) {
    PDU p1,p2;
    int errorState, res;
    p1.header=dc->msgOut+dc->PDUstartO;
    davePrepareWriteRequest(dc, &p1);
    daveAddVarToWriteRequest(&p1, area, DB, start, len, buffer);
    errorState=_daveExchange(dc, &p1);
    res=_daveSetupReceivedPDU(dc, &p2);
    if (res!=0) return res;
    if (p2.param[0]==daveFuncWrite) {
	return errorState;    
    } else {
	return errorState|2048;
    }
}

int DECL2 daveWriteBits(daveConnection * dc,int area, int DB, int start, int len, void * buffer) {
    PDU p1,p2;
    int errorState,res;
    p1.header=dc->msgOut+dc->PDUstartO;
    _daveConstructBitWriteRequest(&p1, area, DB, start, len, buffer);
    errorState=_daveExchange(dc, &p1);
    res=_daveSetupReceivedPDU(dc, &p2);
    if (res!=0) return res;
    if (p2.param[0]==daveFuncWrite) {
	return errorState;    
    } else {
	return errorState|2048;
    }
}


int DECL2 initUpload(daveConnection * dc,char blockType, int blockNr, int * uploadID){
    PDU p1,p2;
    int errorState;
    p1.header=dc->msgOut+dc->PDUstartO;
    _daveConstructUpload(&p1, blockType, blockNr);
    errorState=_daveExchange(dc, &p1);
    if (daveDebug & daveDebugUpload) {
        LOG2("error:%d\n",errorState);
        FLUSH;
    }	
    if (errorState) goto error;
    _daveSetupReceivedPDU(dc, &p2);
    * uploadID=p2.param[7];
    errorState=0x100*p2.header[10]+p2.header[11];
error:    
    return errorState;
}


int DECL2 doUpload(daveConnection*dc, int * more, uc**buffer, int*len, int uploadID){
    PDU p1,p2;
    int errorState,netLen;
    p1.header=dc->msgOut+dc->PDUstartO;
    _daveConstructDoUpload(&p1, uploadID);
    errorState=_daveExchange(dc, &p1);
    if (daveDebug & daveDebugUpload) {
	LOG2("error:%d\n",errorState);
	FLUSH;
    }	
    *more=0;
    if (errorState) goto error;
    _daveSetupReceivedPDU(dc, &p2);
    *more=p2.param[1];
    netLen=p2.data[1] /* +256*p2.data[0]; */ /* for long PDUs, I guess it is so */;
    if (*buffer) {
	memcpy(*buffer,p2.data+4,netLen);
	*buffer+=netLen;
	if (daveDebug & daveDebugUpload) {
	    LOG2("buffer:%p\n",*buffer);
	    FLUSH;
	}    
    }
    *len+=netLen;
error:
    return errorState;
}

int DECL2 endUpload(daveConnection*dc, int uploadID){
    PDU p1,p2;
    int errorState;
    
    p1.header=dc->msgOut+dc->PDUstartO;
    _daveConstructEndUpload(&p1,uploadID);
    
    errorState=_daveExchange(dc, &p1);
    if (daveDebug & daveDebugUpload) {
        LOG2("error:%d\n",errorState);
        FLUSH;
    }	
    if (errorState) goto error;
    _daveSetupReceivedPDU(dc, &p2);
error:
    return errorState;
}

/*
    error code to message string conversion:
*/
char * DECL2 daveStrerror(int code) {
    switch (code) {
	case daveResOK: return "ok";
	case daveResMultipleBitsNotSupported:return "the CPU does not support reading a bit block of length<>1";
	case daveResItemNotAvailable: return "the desired item is not available in the PLC";
	case daveResItemNotAvailable200: return "the desired item is not available in the PLC (200 family)";
	case daveAddressOutOfRange: return "the desired address is beyond limit for this PLC";
	case daveResCPUNoData : return "the PLC returned a packet with no result data";
	case daveUnknownError : return "the PLC returned an error code not understood by this library";
	case daveEmptyResultError : return "this result contains no data";
	case daveEmptyResultSetError: return "cannot work with an undefined result set";
	case daveResCannotEvaluatePDU: return "cannot evaluate the received PDU";
	case daveWriteDataSizeMismatch: return "Write data size error";
/*
    error code to message string conversion:
*/
	default: return "no message defined!";
    }
}

#ifdef HANDMADE_CONVERSIONS
float DECL2 daveGetFloat(daveConnection * dc) {
    union {
	float a;
	uc b[4];
    } f;
    f.b[3]=*((uc*)dc->resultPointer)++;
    f.b[2]=*((uc*)dc->resultPointer)++;
    f.b[1]=*((uc*)dc->resultPointer)++;
    f.b[0]=*((uc*)dc->resultPointer)++;
    return (f.a);
}

int DECL2 daveGetByte(daveConnection * dc){
    return *((uc*)dc->resultPointer)++;
}


int DECL2 daveGetInteger(daveConnection * dc) {
    union {
	int a;
	uc b[4];
    } f;
    f.b[3]=*((uc*)dc->resultPointer)++;
    f.b[2]=*((uc*)dc->resultPointer)++;
    f.b[1]=*((uc*)dc->resultPointer)++;
    f.b[0]=*((uc*)dc->resultPointer)++;
    return (f.a);
}

unsigned DECL2 int daveGetDWORD(daveConnection * dc) {
    union {
	unsigned int a;
	uc b[4];
    } f;
    f.b[3]=*((uc*)dc->resultPointer)++;
    f.b[2]=*((uc*)dc->resultPointer)++;
    f.b[1]=*((uc*)dc->resultPointer)++;
    f.b[0]=*((uc*)dc->resultPointer)++;
    return (f.a);
}    

unsigned DECL2 int daveGetUnsignedInteger(daveConnection * dc) {
    union {
	unsigned int a;
	uc b[4];
    } f;
    f.b[3]=*((uc*)dc->resultPointer)++;
    f.b[2]=*((uc*)dc->resultPointer)++;
    f.b[1]=*((uc*)dc->resultPointer)++;
    f.b[0]=*((uc*)dc->resultPointer)++;
    return (f.a);
}   

unsigned int DECL2 daveGetWORD(daveConnection * dc) {
    union {
	short a;
	uc b[2];
    } f;
    f.b[1]=*((uc*)dc->resultPointer)++;
    f.b[0]=*((uc*)dc->resultPointer)++;
    return (f.a);
}    

int DECL2 daveGetByteat(daveConnection * dc, int pos) {
    uc* p=(uc*)dc->_resultPointer;
    p+=pos;
    return *p;
}


unsigned int DECL2 daveGetWORDat(daveConnection * dc, int pos) {
    union {
	short a;
	uc b[2];
    } f;
    uc* p=(uc*)dc->_resultPointer;
    p+=pos;
    f.b[1]=*p;p++;
    f.b[0]=*p;
    return (f.a);
}

unsigned int DECL2 daveGetDWORDat(daveConnection * dc, int pos) {
    union {
	unsigned int a;
	uc b[4];
    } f;
    uc* p=(uc*)dc->_resultPointer;
    p+=pos;
    f.b[0]=*p;p++;
    f.b[1]=*p;p++;
    f.b[2]=*p;p++;
    f.b[3]=*p;
    return (f.a);
}

float DECL2 daveGetFloatat(daveConnection * dc, int pos) {
    union {
	float a;
	uc b[4];
    } f;
    uc* p=(uc*)dc->_resultPointer;
    p+=pos;
    f.b[3]=*p;p++;
    f.b[2]=*p;p++;
    f.b[1]=*p;p++;
    f.b[0]=*p;
    return (f.a);
}

float DECL2 toPLCfloat(float ff) {
    union {
	float a;
	uc b[4];
    } f;
    uc c;
    f.a=ff;
    c=f.b[0];
    f.b[0]=f.b[3];
    f.b[3]=c;
    c=f.b[1];
    f.b[1]=f.b[2];
    f.b[2]=c;
    return (f.a);
}

short DECL2 bswap_16(short ff) {
    union {
	short a;
	uc b[2];
    } f;
    uc c;
    f.a=ff;
    c=f.b[0];
    f.b[0]=f.b[1];
    f.b[1]=c;
    return (f.a);
}

int DECL2 bswap_32(int ff) {
    union {
	int a;
	uc b[4];
    } f;
    uc c;
    f.a=ff;
    c=f.b[0];
    f.b[0]=f.b[3];
    f.b[3]=c;
    c=f.b[1];
    f.b[1]=f.b[2];
    f.b[2]=c;
    return (f.a);
}

#endif

/**
    Timer and Counter conversion functions:
**/
/*	
    get time in seconds from current read position:
*/
float DECL2 daveGetSeconds(daveConnection * dc) {
    uc b[2],a;
    float f;
    b[1]=*(dc->resultPointer)++;
    b[0]=*(dc->resultPointer)++;
    f=b[0] & 0xf;
    f+=10*((b[0] & 0xf0)>>4);
    f+=100*(b[1] & 0xf);
    a=((b[1] & 0xf0)>>4);
    switch (a) {
	case 0: f*=0.01;break;
	case 1: f*=0.1;break;
	case 3: f*=10.0;break;
    }
    return (f);    
}
/*	
    get time in seconds from random position:
*/
float DECL2 daveGetSecondsAt(daveConnection * dc, int pos) {
    float f;
    uc b[2],a;
    uc* p=(uc*)dc->_resultPointer;
    p+=pos;
    b[1]=*p;
    p++;
    b[0]=*p;
    f=b[0] & 0xf;
    f+=10*((b[0] & 0xf0)>>4);
    f+=100*(b[1] & 0xf);
    a=((b[1] & 0xf0)>>4);
    switch (a) {
	case 0: f*=0.01;break;
	case 1: f*=0.1;break;
	case 3: f*=10.0;break;
    }
    return (f);
}
/*	
    get counter value from current read position:
*/
int DECL2 daveGetCounterValue(daveConnection * dc) {
    uc b[2];
    int f;
    b[1]=*(dc->resultPointer)++;
    b[0]=*(dc->resultPointer)++;
    f=b[0] & 0xf;
    f+=10*((b[0] & 0xf0)>>4);
    f+=100*(b[1] & 0xf);
    return (f);    
}
/*	
    get counter value from random read position:
*/
int DECL2 daveGetCounterValueAt(daveConnection * dc,int pos){
    int f;
    uc b[2];
    uc* p=(uc*)dc->_resultPointer;
    p+=pos;
    b[1]=*p;
    p++;
    b[0]=*p;
    f=b[0] & 0xf;
    f+=10*((b[0] & 0xf0)>>4);
    f+=100*(b[1] & 0xf);
    return (f);
}


/*
    dummy functions for protocols not providing a specific function:
*/

int DECL2 _daveReturnOkDummy(void * dummy){
    return 0;
}

int DECL2 _daveListReachablePartnersDummy (daveInterface * di, char * buf) {
    return 0;
}

/*
    MPI specific functions:
*/

/* 
    This writes a single chracter to the serial interface:
*/

void DECL2 _daveSendSingle(daveInterface * di,	/* serial interface */
		     uc c  			/* chracter to be send */
		     ) 
{
    unsigned long i;
    daveWriteFile(di->fd.wfd, &c, 1, i);
}

#ifdef HAVE_SELECT
int DECL2 _daveReadSingle(daveInterface * di) {
	fd_set FDS;
	struct timeval t;
	char res;
	t.tv_sec = 1;
	t.tv_usec = 10;
	FD_ZERO(&FDS);
	FD_SET(di->fd.rfd, &FDS);
	if(select(di->fd.rfd + 1, &FDS, NULL, NULL, &t)>0) {
	    if (read(di->fd.rfd, &res, 1)==1) return res;
	}    
	return 0;
}
#endif 

#ifdef BCCWIN
int DECL2 _daveReadSingle(daveInterface * di) {
    unsigned long i;
    char res;
    ReadFile(di->fd.rfd, &res, 1, &i,NULL);
    if ((daveDebug & daveDebugSpecialChars)!=0)
	LOG3("readSingle %d chars. 1st %02X\n",i,res);
    if(i==1){
	 return res;
    } else {	 
	return 0;
    }	
}
#endif 

#ifdef HAVE_SELECT
int DECL2 _daveReadMPI(daveInterface * di, uc *b) {
	fd_set FDS;
	struct timeval t;
	int charsAvail;
	int res=0,state=0;
	uc bcc=0;
rep:	
	t.tv_sec = 1;
	t.tv_usec = 10;
	FD_ZERO(&FDS);
	FD_SET(di->fd.rfd, &FDS);
	charsAvail = select(di->fd.rfd + 1, &FDS, NULL, NULL, &t);
	if (!charsAvail) {
	    LOG1("timeout !\n");
	    return (0);
	} else {
	    res+= read(di->fd.rfd, b+res, 1);
	    if ((res==1) && (*(b+res-1)==DLE)) {
		if ((daveDebug & daveDebugSpecialChars)!=0)
		    LOG1("readMPI single DLE.\n");
		return 1;
	    }		
	    if ((res==1) && (*(b+res-1)==STX)) {
		if ((daveDebug & daveDebugSpecialChars)!=0)
		    LOG1("readMPI single STX.\n");
	        return 1;
	    }
	    if (*(b+res-1)==DLE) {
		if (state==0) {
		    state=1;
		    if ((daveDebug & daveDebugSpecialChars)!=0)
/*			LOG1("readMPI 1st DLE in data.\n") */
			;
		} else if (state==1) {
		    state=0;
		    res--;		/* forget this DLE */
		    if ((daveDebug & daveDebugSpecialChars)!=0)
/*		        LOG1("readMPI 2nd DLE in data.\n") */
		    ;
		}
	    } 	
	    if (state==3) {
	        if ((daveDebug & daveDebugSpecialChars)!=0)
			LOG3("readMPI: packet end, got BCC: %x. I calc: %x\n",*(b+res-1),bcc);
		if ((daveDebug & daveDebugRawRead)!=0)	
		    _daveDump("answer",b,res);	
		return res;	    				
	    } else {
		bcc=bcc^(*(b+res-1));
	    }
	    	
	    if (*(b+res-1)==ETX) if (state==1) {
	        state=3;
	        if ((daveDebug & daveDebugSpecialChars)!=0)
		    LOG1("readMPI: DLE ETX,packet end.\n");
	    }	
	    goto rep;
	} 
    }
#endif

#ifdef BCCWIN
int DECL2 _daveReadMPI(daveInterface * di, uc *b) {
	int charsAvail, res=0,state=0;
	unsigned long i;
	uc bcc=0;
rep:	
	ReadFile(di->fd.rfd, b+res, 1, &i, NULL);
	charsAvail = (i>0);
	if (!charsAvail) {
	    LOG1("timeout !\n");
	    return (0);
	} else {
	    res+= i;
	    if ((res==1) && (*(b+res-1)==DLE)) {
		if ((daveDebug & daveDebugSpecialChars)!=0)
		    LOG1("readMPI single DLE.\n");
		return 1;
	    }		
	    if ((res==1) && (*(b+res-1)==STX)) {
		if ((daveDebug & daveDebugSpecialChars)!=0)
		    LOG1("readMPI single STX.\n");
	        return 1;
	    }
	    if (*(b+res-1)==DLE) {
		if (state==0) {
		    state=1;
		    if ((daveDebug & daveDebugSpecialChars)!=0)
/*			LOG1("readMPI 1st DLE in data.\n") */
			;
		} else if (state==1) {
		    state=0;
		    res--;		/* forget this DLE */
		    if ((daveDebug & daveDebugSpecialChars)!=0)
/*		        LOG1("readMPI 2nd DLE in data.\n") */
		    ;
		}
	    } 	
	    if (state==3) {
	        if ((daveDebug & daveDebugSpecialChars)!=0)
			LOG3("readMPI: packet end, got BCC: %x. I calc: %x\n",*(b+res-1),bcc);
		if ((daveDebug & daveDebugRawRead)!=0)	
		    _daveDump("answer",b,res);	
		return res;	    				
	    } else {
		bcc=bcc^(*(b+res-1));
	    }
	    	
	    if (*(b+res-1)==ETX) if (state==1) {
	        state=3;
	        if ((daveDebug & daveDebugSpecialChars)!=0)
		    LOG1("readMPI: DLE ETX,packet end.\n");
	    }	
	    goto rep;
	} 
    }
#endif

int DECL2 _daveReadMPI2(daveInterface * di, uc *b) {
	int res=_daveReadMPI(di, b);
	if (res>1) {
		_daveSendSingle(di, DLE);
		_daveSendSingle(di, STX);
	}
	return res;
}

int DECL2 _daveGetAck(daveInterface*di, int nr) {
    int res;    
    uc b1[daveMaxRawLen];
    if (daveDebug & daveDebugPacket)
	LOG2("%s enter getAck ack\n", di->name);
    res = _daveReadMPI(di, b1);
/*    LOG2("getAck ack got %d bytes.\n", res); */
    if (res<0) return res-10;
    if ((res!=di->ackPos+6) /*&& ((res!=13) && (nr==DLE))*/) {	/* fixed by Andrew Rostovtsew */
	if (daveDebug & daveDebugPrintErrors) {
    	    LOG4("%s *** getAck wrong length %d for ack. Waiting for %d\n dump:", di->name, res, nr);
	    _daveDump("wrong ack:",b1,res);
	}
	return -1;
    }	
    if (b1[di->ackPos]!=0xB0) {
	if (daveDebug & daveDebugPrintErrors) {
    	    LOG3("%s *** getAck char[6] %x no ack\n", di->name, b1[di->ackPos+2]);
	}    
	return -2;
    }		
    if (b1[di->ackPos+2]!=nr) {
	if (daveDebug & daveDebugPrintErrors) {
    	    LOG4("%s *** getAck got: %d need: %d\n", di->name, b1[di->ackPos+2],nr);
	}    
	return -3;
    }	
/*    LOG4("%s *** getAck got: %d need: %d\n", di->name, b1[di->ackPos+2],nr); */
    return 0;
}


#define tmo_normal 95000

#ifdef HAVE_UNISTD
/* 
    This reads as many chracters as it can get and returns the number:
*/
int DECL2 _daveReadChars(daveInterface * di,	/* serial interface */
		    uc *b, 		/* a buffer */
		    int tmo,		/* timeout in us */
		    int max		/* limit */
		)
{
    int charsAvail;
    int res;
    fd_set FDS;         
    struct timeval t;
    t.tv_sec = 0;
    t.tv_usec = tmo;
    FD_ZERO(&FDS);
    FD_SET(di->fd.rfd, &FDS);
    charsAvail = (select(di->fd.rfd + 1, &FDS, NULL, NULL, &t) > 0);
	        
    if (!charsAvail) {
	if ((daveDebug & daveDebugRawRead)!=0)
	    LOG2("%s timeout !\n", di->name);
	return (0);
    } else {
	if ((daveDebug & daveDebugRawRead)!=0)
	    LOG3("%s _daveReadChars reading up to %d chars.\n", di->name,max);
	res = read(di->fd.rfd, b, max);
	return (res);
    }	
}

#else
#ifdef BCCWIN
/* 
    This reads as many chracters as it can get and returns the number:
*/
int DECL2 _daveReadChars(daveInterface * di,	/* serial interface */
		    uc *b, 		/* a buffer */
		    int tmo,		/* timeout in us */
		    int max		/* limit */
		)
{
    unsigned long res;
    setTimeOut(di, tmo);
    
    ReadFile(di->fd.rfd, b, max, &res, NULL);
    if (res<=0) {
	if ((daveDebug & daveDebugRawRead)!=0)
	    LOG2("%s timeout !\n", di->name);
	return (0);
    } else {
	if ((daveDebug & daveDebugRawRead)!=0)
	    LOG3("%s _daveReadChars reading up to %d chars.\n", di->name,max);
	return (res);
    }	
}

#else
#error put the code for receive with time limit for your OS here!    
#endif 
#endif

/* 
    This sends a string after doubling DLEs in the String
    and adding DLE,ETX and bcc.
*/
int DECL2 _daveSendWithCRC(daveInterface * di, /* serial interface */
		    uc *b, 		 /* a buffer containing the message */
		    int size		 /* the size of the string */
		)
{		
    uc target[daveMaxRawLen];	
    unsigned long wr;
    int i,targetSize=0;
    int bcc=DLE^ETX; /* preload */
    for (i=0; i<size; i++) {
	target[targetSize]=b[i];targetSize++;
	if (DLE==b[i]) {
	    target[targetSize]=DLE;
	    targetSize++;
	}  else 
	    bcc=bcc^b[i];	/* The doubled DLE effectively contributes nothing */
    };
    target[targetSize]=DLE;
    target[targetSize+1]=ETX;
    target[targetSize+2]=bcc;
    targetSize+=3;
    daveWriteFile(di->fd.wfd, target, targetSize, wr);
    if (daveDebug & daveDebugPacket)
	    _daveDump("_daveSendWithCRC",target, targetSize);
    return 0;
}


/* 
    This adds a prefix to a string and theen sends it
    after doubling DLEs in the String
    and adding DLE,ETX and bcc.
*/
int DECL2 _daveSendWithPrefix(daveConnection * dc, uc *b, int size)
{
    uc target[daveMaxRawLen];
    uc fix[]= {04,0x80,0x80,0x0C,0x03,0x14};
    uc fix2[]= {0x00,0x0c,0x03,0x03};
/*    if (daveDebug & daveDebugPacket)
	LOG3("%s enter _daveSendWithPrefix %d chars.\n", dc->iface->name,size); */
    if (dc->iface->protocol==daveProtoMPI2) {	
	fix2[2]=dc->connectionNumber2; 		// 1/10/05 trying Andrew's patch
	fix2[3]=dc->connectionNumber; 		// 1/10/05 trying Andrew's patch
	memcpy(target,fix2,sizeof(fix2));
	memcpy(target+sizeof(fix2),b,size);
	return _daveSendWithCRC(dc->iface,target,size+sizeof(fix2));
    }  else {
	fix[4]=dc->connectionNumber2; 		// 1/10/05 trying Andrew's patch
	fix[5]=dc->connectionNumber; 		// 1/10/05 trying Andrew's patch
    	memcpy(target,fix,sizeof(fix));
	memcpy(target+sizeof(fix),b,size);
	target[1]|=dc->MPIAdr;
//	target[2]|=dc->iface->localMPI;
	memcpy(target+sizeof(fix),b,size);
	return _daveSendWithCRC(dc->iface,target,size+sizeof(fix));
    }	
}

int DECL2 _daveSendWithPrefix2(daveConnection * dc, int size)
{
    uc fix[]= {04,0x80,0x80,0x0C,0x03,0x14};
    uc fix2[]= {0x00, 0x0C, 0x03, 0x03};
    
    if (dc->iface->protocol==daveProtoMPI2) {
	fix2[2]=dc->connectionNumber2; 		// 1/10/05 trying Andrew's patch
	fix2[3]=dc->connectionNumber; 		// 1/10/05 trying Andrew's patch
	memcpy(dc->msgOut, fix2, sizeof(fix2));
	dc->msgOut[sizeof(fix2)]=0xF1;
/*	if (daveDebug & daveDebugPacket)
	    _daveDump("_daveSendWithPrefix2",dc->msgOut, size+sizeof(fix2)); */
	return _daveSendWithCRC(dc->iface, dc->msgOut, size+sizeof(fix2));
    }
    else if (dc->iface->protocol==daveProtoMPI) {
	fix[4]=dc->connectionNumber2;		// 1/10/05 trying Andrew's patch
	fix[5]=dc->connectionNumber;		// 1/10/05 trying Andrew's patch
	memcpy(dc->msgOut, fix, sizeof(fix));
	dc->msgOut[1]|=dc->MPIAdr;
//	dc->msgOut[2]|=dc->iface->localMPI; //???
	dc->msgOut[sizeof(fix)]=0xF1;
/*	if (daveDebug & daveDebugPacket)
	    _daveDump("_daveSendWithPrefix2",dc->msgOut,size+sizeof(fix)); */
	return _daveSendWithCRC(dc->iface, dc->msgOut, size+sizeof(fix));
    }
    return -1; /* shouldn't happen. */
}

/* 
    Sends an ackknowledge message for the message number nr:
*/
int DECL2 _daveSendAck(daveConnection * dc, int nr)
{
    uc m[3];
    if (daveDebug & daveDebugPacket)
	LOG3("%s sendAck for message %d \n", dc->iface->name,nr);
    m[0]=0xB0;
    m[1]=0x01;
    m[2]=nr;
    return _daveSendWithPrefix(dc, m, 3);
}

/*
    Executes part of the dialog necessary to send a message:
*/
int DECL2 _daveSendDialog2(daveConnection * dc, int size)
{
    int a;
    _daveSendSingle(dc->iface, STX);
    if (_daveReadSingle(dc->iface)!=DLE) {
        LOG2("%s *** no DLE before send.\n", dc->iface->name);	    
	return -1;
    } 
    if (size>5){
//	if (dc->messageNumber==0) dc->messageNumber=1;
	dc->msgOut[dc->iface->ackPos+1]=(dc->messageNumber);	
	if(dc->msgOut[dc->iface->ackPos+6]==0)		/* do not number already numbered PDUs 12/10/04 */
	    dc->msgOut[dc->iface->ackPos+6]=(dc->messageNumber+1)&0xff;
	dc->needAckNumber=dc->messageNumber;		
	dc->messageNumber++;	
	dc->messageNumber&=0xff;	/* !! */
	if (dc->messageNumber==0) dc->messageNumber=1;
    }	
    _daveSendWithPrefix2(dc, size);
    a=_daveReadSingle(dc->iface);
    if (a!=DLE) {
	LOG3("%s *** no DLE after send(1) %02x.\n", dc->iface->name,a);
	a=_daveReadSingle(dc->iface);
	if (a!=DLE) {
	    LOG3("%s *** no DLE after send(2) %02x.\n", dc->iface->name,a);
	    _daveSendWithPrefix2(dc, size);
	    a=_daveReadSingle(dc->iface);
	    if (a!=DLE) {
		LOG3("%s *** no DLE after resend(3) %02x.\n", dc->iface->name,a);
		_daveSendSingle(dc->iface, STX);
		a=_daveReadSingle(dc->iface);
		if (a!=DLE) {
    		    LOG2("%s *** no DLE before resend.\n", dc->iface->name);	    
		    return -1;
		} else {
		    _daveSendWithPrefix2(dc, size);
		    a=_daveReadSingle(dc->iface);
		    if (a!=DLE) {
    			LOG2("%s *** no DLE before resend.\n", dc->iface->name);
			return -1;
		    } else {
			LOG2("%s *** got DLE after repeating whole transmisson.\n", dc->iface->name);
			return 0;
		    }	
		}
		return -2;
	    } else	
		LOG3("%s *** got DLE after resend(3) %02x.\n", dc->iface->name,a);
	}    
	
    }
    return 0;
}

int DECL2 _daveGetResponseMPI(daveConnection *dc) {
    int res;
    res = 0;
    
    res= _daveReadSingle(dc->iface);
    if (res!=STX) {
	if (daveDebug & daveDebugPrintErrors) {
	    LOG2("%s *** _daveGetResponseMPI no STX before answer data.\n", dc->iface->name);	    
	}        
	res= _daveReadSingle(dc->iface);
    }
    _daveSendSingle(dc->iface,DLE);
    if (daveDebug & daveDebugExchange) {
        LOG2("%s _daveGetResponseMPI receive message.\n", dc->iface->name);	    
    }	
    res = _daveReadMPI2(dc->iface,dc->msgIn);
/*	LOG3("%s *** _daveExchange read result %d.\n", dc->iface->name, res); */
    if (res<=0) {
	if (daveDebug & daveDebugPrintErrors) {
	    LOG2("%s *** _daveGetResponseMPI no answer data.\n", dc->iface->name);	    
	}        
	return -3;
    }	
    if (daveDebug & daveDebugExchange) {
        LOG3("%s _daveGetResponseMPI got %d bytes\n", dc->iface->name, dc->AnswLen);	    
    }    
    if (_daveReadSingle(dc->iface)!=DLE) {
        if (daveDebug & daveDebugPrintErrors) {
            LOG2("%s *** _daveGetResponseMPI: no DLE.\n", dc->iface->name);	    
        }	
	return -5;
    }    
    _daveSendAck(dc, dc->msgIn[dc->iface->ackPos+1]);
    if (_daveReadSingle(dc->iface)!=DLE) {
        if (daveDebug & daveDebugPrintErrors) {
            LOG2("%s *** _daveGetResponseMPI: no DLE after Ack.\n", dc->iface->name);	    
        }
	return -6;
    }    
    return 0;
}

/*
    Sends a message and gets ackknowledge:
*/
int DECL2 _daveSendMessageMPI(daveConnection * dc, PDU * p) {
    int res;
    if (daveDebug & daveDebugExchange) {
        LOG2("%s enter _daveExchangeMPI\n", dc->iface->name);	    
    }    
    if (_daveSendDialog2(dc, 2+p->hlen+p->plen+p->dlen)) {
	LOG2("%s *** _daveExchangeMPI error in _daveSendDialog.\n",dc->iface->name);	    		
//	return -1;	
    }	
    if (daveDebug & daveDebugExchange) {
        LOG3("%s _daveExchangeMPI send done. needAck %x\n", dc->iface->name,dc->needAckNumber);	    
    }	
    
    if (_daveReadSingle(dc->iface)!=STX) {
	if (daveDebug & daveDebugPrintErrors) {
	    LOG2("%s *** _daveExchangeMPI no STX after _daveSendDialog.\n",dc->iface->name);
	}    
	if ( _daveReadSingle(dc->iface)!=STX) {
	    if (daveDebug & daveDebugPrintErrors) {
		LOG2("%s *** _daveExchangeMPI no STX after _daveSendDialog.\n",dc->iface->name);
	    }	
	    return -2;
	} else {
	    if (daveDebug & daveDebugPrintErrors) {
		LOG2("%s *** _daveExchangeMPI got STX after retry.\n",dc->iface->name);
	    }
	}    
    }
    _daveSendSingle(dc->iface,DLE);
/*    LOG2("%s *** _daveExchangeMPI after sendSingle.\n",dc->iface->name); */
    _daveGetAck(dc->iface, dc->needAckNumber);
/*    LOG2("%s *** _daveExchangeMPI after getAck.\n",dc->iface->name); */
    _daveSendSingle(dc->iface,DLE);
/*    LOG2("%s *** _daveExchangeMPI after sendSingle.\n",dc->iface->name); */
}

int DECL2 _daveExchangeMPI(daveConnection * dc, PDU * p) {
    _daveSendMessageMPI(dc, p);
    dc->AnswLen=0;
    return _daveGetResponseMPI(dc);
}

/* 
    Send a string of init data to the MPI adapter.
*/
int DECL2 _daveInitStep(daveInterface * di, int nr, uc *fix, int len, char * caller) {
    int res;
    if (_daveReadSingle(di)!=DLE){
	if (daveDebug & daveDebugInitAdapter)
	    LOG4("%s %s no answer (DLE) from adapter (%d chars).\n", 
		di->name, caller, res);
	return nr;
    }	 
    if (daveDebug & daveDebugInitAdapter)
	LOG4("%s %s step %d.\n", di->name, caller, nr);
    _daveSendWithCRC(di, fix, len);
    if (_daveReadSingle(di)!=DLE) return nr+1;
    if (daveDebug & daveDebugInitAdapter)
	LOG4("%s %s step %d.\n", di->name, caller,nr+1);
    if (_daveReadSingle(di)!=STX) return nr+2;
    if (daveDebug & daveDebugInitAdapter)
	LOG4("%s %s step %d.\n", di->name, caller,nr+2);
    _daveSendSingle(di,DLE);	
    return 0;
}    

/* 
    This initializes the MPI adapter. Andrew's version.
*/
int DECL2 _daveInitAdapterMPI2(daveInterface * di)  /* serial interface */
{
    uc b3[]={
	0x01,0x03,0x02,0x17, 0x00,0x9F,0x01,0x3C,
 	0x00,0x90,0x01,0x14, 0x00,	/* ^^^ MaxTsdr */
	0x00,0x5,
 	0x02,/* Bus speed */

	0x00,0x0F,0x05,0x01,0x01,0x03,0x80,/* from topserverdemo */
	/*^^ - Local mpi */
    };		
    int res;
    uc b1[daveMaxRawLen];
    b3[16]=di->localMPI;
    if (di->speed==daveSpeed500k)
	b3[7]=0x64;
    if (di->speed==daveSpeed1500k)
	b3[7]=0x96;
    b3[15]=di->speed;

    if (daveDebug & daveDebugInitAdapter) 
	LOG2("%s enter initAdapter(2).\n", di->name);
    res = _daveReadChars(di, b1, 3*tmo_normal,daveMaxRawLen);
    /* _daveSendSingle(di,DLE); */
    _daveSendSingle(di,STX);
/*    usleep (40000); */
    res= _daveReadChars(di, b1, 3*tmo_normal,1);
    if ((b1[0]!=DLE)){
	if (daveDebug & daveDebugInitAdapter) 
	    LOG3("%s initAdapter() no answer (DLE) from adapter (%d chars).\n", di->name,res);
	return 1;
    }	 
    if (daveDebug & daveDebugInitAdapter) 
	LOG2("%s initAdapter()  step 1.\n", di->name);
/*    usleep(40000); */
    if (daveDebug & daveDebugInitAdapter) 
	LOG2("%s initAdapter() step 2.\n", di->name);
    res=_daveReadChars(di,b1, 3*tmo_normal,1);
    if ((b1[0]!=STX)&&(b1[0]!=DLE)) return 3;
    if (daveDebug & daveDebugInitAdapter) 
	LOG2("%s initAdapter() step 3.\n", di->name);
    if (daveDebug & daveDebugInitAdapter) 
	LOG2("%s initAdapter() step 4.\n", di->name);
    
    _daveSendWithCRC(di, b3, sizeof(b3));	
    res = _daveReadChars(di, b1, tmo_normal,daveMaxRawLen);
    if (daveDebug & daveDebugInitAdapter) 
	LOG2("%s initAdapter() step 5.\n", di->name);
/*    usleep(40000); */
    res = _daveReadChars(di, b1, tmo_normal,daveMaxRawLen);
    if (daveDebug & daveDebugInitAdapter) 
	LOG2("%s initAdapter() step 6.\n", di->name);
    	
/*    usleep (1000000); */ /* Andrew used sleep(1) here */
/*    usleep (100000); */
        _daveSendSingle(di,DLE);
    res = _daveReadChars(di, b1, tmo_normal,daveMaxRawLen);
    if (daveDebug & daveDebugInitAdapter) 
	LOG2("%s initAdapter() success.\n", di->name);
/*    usleep(40000); */
   _daveSendSingle(di,DLE);
    di->users=0;	/* there cannot be any connections now */
    return 0;
}

/* 
    Initializes the MPI adapter.
*/
int DECL2 _daveInitAdapterMPI1(daveInterface * di) {
    uc b2[]={
	0x01,0x0D,0x02,
    };
    us answ1[]={0x01,0x0D,0x20,'V','0','0','.','8','3'};
    us adapter0330[]={0x01,0x03,0x20,'E','=','0','3','3','0'};
    us answ2[]={0x01,0x03,0x20,'V','0','0','.','8','3'};
/*    1,3,2,27,0,9f,1,3c,0,90,1,14,0 */
/* ,0,5, */
/* 2,0,1f,2,1,1,3,81,	*/
    
    uc b3[]={
/*    
	0x01,0x03,0x02,0x27, 0x00,0x9F,0x01,0x3C,
	0x00,0x90,0x01,0x14, 0x00,
	0x00,0x05,
	0x02,
	0x00,0x1F,0x02,0x01,0x01,0x03,0x80,
*/	
	0x01,0x03,0x02,0x27, 0x00,0x9F,0x01,0x3C,
	0x00,0x90,0x01,0x14, 0x00,
	0x00,0x05,
	0x02,
	0x00,0x1F,0x02,0x01,0x01,0x03,0x80,
	
//	1,3,2,27,
//	0,9f,1,3c,
//	0,90,1,14,0,0,5,2,
//	6,1f,2,1,1,3,80,10,3,8c,
//	^localMPI
    };		
    uc v1[]={
	0x01,0x0C,0x02,
    };
    int res;
    uc b1[daveMaxRawLen];
    if (daveDebug & daveDebugInitAdapter)
	LOG2("%s enter initAdapter(1).\n", di->name);
	
    _daveSendSingle(di,STX);
    
    res=_daveInitStep(di, 1, b2, sizeof(b2),"initAdapter()");
    if (res) {
	if (daveDebug & daveDebugInitAdapter)
	    LOG2("%s initAdapter() fails.\n", di->name);
	return -44;    
    }	    
    
    res= _daveReadMPI2(di, b1);
    
    if (_daveMemcmp(answ1, b1, sizeof(answ1)/2)) return 4;
    
    b3[16]=di->localMPI;

    if (di->speed==daveSpeed500k)
	b3[7]=0x64;
    if (di->speed==daveSpeed1500k)
	b3[7]=0x96;
    b3[15]=di->speed;
//    b3[sizeof(b3)-1]^=di->localMPI; /* 'patch' the checksum */
    res=_daveInitStep(di, 4, b3, sizeof(b3),"initAdapter()");	
    if (res) {
	if (daveDebug & daveDebugInitAdapter)
	    LOG2("%s initAdapter() fails.\n", di->name);
	return -54;    
    }
/*
    The following extra lines seem to be necessary for 
    TS adapter 6ES7 972-0CA33-0XAC:
*/        
    res= _daveReadMPI(di, b1);
    _daveSendSingle(di,DLE);    
    if (!_daveMemcmp(adapter0330, b1, sizeof(adapter0330)/2)) {
	if (daveDebug & daveDebugInitAdapter)
	    LOG2("%s initAdapter() found Adapter E=0330.\n", di->name);
	_daveSendSingle(di,STX);
	res= _daveReadMPI2(di, b1);
	_daveSendWithCRC(di, v1, sizeof(v1));	
	if (daveDebug & daveDebugInitAdapter)
	    LOG2("%s initAdapter() Adapter E=0330 step 7.\n", di->name);
	if (_daveReadSingle(di)!=DLE) return 8;
	if (daveDebug & daveDebugInitAdapter)
	    LOG2("%s initAdapter() Adapter E=0330 step 8.\n", di->name);
	res= _daveReadMPI(di, b1);
/*	if (_daveReadSingle(di)!=STX) return 9; */
	if (res!=1 || b1[0]!=STX) return 9;
	if (daveDebug & daveDebugInitAdapter)
	    LOG2("%s initAdapter() Adapter E=0330 step 9.\n", di->name);
	_daveSendSingle(di,DLE);
/* This needed the exact Adapter version:    */
/* instead, just read and waste it */ 
	res= _daveReadMPI(di, b1);
	if (daveDebug & daveDebugInitAdapter)
	    LOG2("%s initAdapter() Adapter E=0330 step 10.\n", di->name);
	_daveSendSingle(di,DLE);    
	return 0;    
    
    } else if (!_daveMemcmp(answ2, b1, sizeof(answ2)/2)) {
	if (daveDebug & daveDebugInitAdapter)
	    LOG2("%s initAdapter() success.\n", di->name);
	di->users=0;	/* there cannot be any connections now */
	return 0;
    } else {
	if (daveDebug & daveDebugInitAdapter)
	    LOG2("%s initAdapter() failed.\n", di->name);
	return -56;    
    }
}

int DECL2 _daveReadS7Packet(daveInterface * di,uc *b) {
	int i,res=0,length;
	res+=_daveReadOne(di,b);
	res+=_daveReadOne(di,b+res);
	res+=_daveReadOne(di,b+res);
	res+=_daveReadOne(di,b+res);
	res+=_daveReadOne(di,b+res);
	if ((res>0)&&(res<5)) res+=_daveReadOne(di,b+res);
	if ((res>0)&&(res<5)) res+=_daveReadOne(di,b+res);
	if ((res>0)&&(res<5)) res+=_daveReadOne(di,b+res);
        if (res<5) {
	    if (daveDebug & daveDebugByte) {
		LOG2("res %d ",res);
		_daveDump("readS7packet: short packet", b, res);
	    }
	    return (0); /* short packet */
	}
	if(b[0]==(b[4]))
	    LOG3("readS7packet: keep alive? %d,%d\n",res, length);
	if(b[2]==(0xFF^b[3])) {
	    length=b[2]+7;
	    LOG3("readS7packet: message %d, %d\n",res, length);
	} else {
	    LOG3("readS7packet: keep alive? %d,%d\n",res, length);
	}    
	while (res<length) {
	    i=_daveReadOne(di, b+res); /* length +8 -3 already read */
	    if (i<1) {
		if (daveDebug & daveDebugByte) 
		    LOG3("readS7packet: %d bytes read, %d needed\n",res, length);
		 return (0);
	    }    	 
	    res+=i;
	} 
	if (daveDebug & daveDebugByte) {
	    LOG3("readS7packet: %d bytes read, %d needed\n",res, length);
	    _daveDump("readS7packet: packet", b, res);    
	}    
	return (res);
};

/* 
    This initializes the MPI adapter. Step 7 version.
*/
/*
int DECL2 _daveInitAdapterMPI3(daveInterface * di)  / * serial interface * /
{
/ *
    uc b2[]={
	0x01,0x0D,0x02,
    };
* /    
    unsigned long i;
    us answ1[]={0x01,0x0D,0x20,'V','0','0','.','8','3'};
    us adapter0330[]={0x01,0x03,0x20,'E','=','0','3','3','0'};
    us answ2[]={0x01,0x03,0x20,'V','0','0','.','8','3'};
/ *    1,3,2,27,0,9f,1,3c,0,90,1,14,0 * /
/ * ,0,5, * /
/ * 2,0,1f,2,1,1,3,81,	* /
    
    uc b3[]={
/ *    
	0x01,0x03,0x02,0x27, 0x00,0x9F,0x01,0x3C,
	0x00,0x90,0x01,0x14, 0x00,
	0x00,0x05,
	0x02,
	0x00,0x1F,0x02,0x01,0x01,0x03,0x80,
* /	
	0x01,0x03,0x02,0x27, 0x00,0x9F,0x01,0x3C,
	0x00,0x90,0x01,0x14, 0x00,
	0x00,0x05,
	0x02,
	0x00,0x1F,0x02,0x01,0x01,0x03,0x80,
//	1,3,2,27,
//	0,9f,1,3c,
//	0,90,1,14,0,0,5,2,
//	6,1f,2,1,1,3,80,10,3,8c,
//	^localMPI
    };		
    uc o1[]={DLE,0x7e,0xfc,0x9b,0xcd,0x7e};
    uc o2[]={0x7e,0,3,0xfc,1,7,2,0x9a,0x3e,0x7e};
    uc v1[]={
	0x01,0x0C,0x02,
    };
    int res;
    uc b1[daveMaxRawLen];
    if (daveDebug & daveDebugInitAdapter)
	LOG2("%s enter initAdapterMPI3().\n", di->name);
	
    _daveSendSingle(di,STX);
/ *    
    res=_daveInitStep(di, 1, b2, sizeof(b2),"initAdapter()");
    if (res) {
	if (daveDebug & daveDebugInitAdapter)
	    LOG2("%s initAdapter() fails.\n", di->name);
	return -44;    
    }	    
    
    res= _daveReadMPI2(di, b1);
    
    if (_daveMemcmp(answ1, b1, sizeof(answ1)/2)) return 4;
* /    
    b3[16]=di->localMPI;

    if (di->speed==daveSpeed500k)
	b3[7]=0x64;
    if (di->speed==daveSpeed1500k)
	b3[7]=0x96;
    b3[15]=di->speed;
//    b3[sizeof(b3)-1]^=di->localMPI; / * 'patch' the checksum * /
    res=_daveInitStep(di, 4, b3, sizeof(b3),"initAdapter()");	
    if (res) {
	if (daveDebug & daveDebugInitAdapter)
	    LOG2("%s initAdapter() fails.\n", di->name);
	return -54;    
    }
    res= _daveReadMPI(di, b1);
    
    res=daveWriteFile(di->fd.wfd, o1, sizeof(o1),i);	
    
    res=_daveReadChars(di, b1, di->timeout, 1024);	
    if (daveDebug & daveDebugInitAdapter) {
	    LOG3("%s initAdapter() %d chars.\n", di->name, res);
	    _daveDump("got:",b1,res);
    }
/ *    
    res=daveWriteFile(di->fd.wfd, o2, sizeof(o2),i);
    
    res=_daveReadChars(di, b1, di->timeout, 1024);	
    if (daveDebug & daveDebugInitAdapter) {
	    LOG3("%s initAdapter() %d chars.\n", di->name, res);
	    _daveDump("got:",b1,res);
    }
    
    res=_daveReadS7Packet(di, b1);	
    res=_daveReadChars(di, b1, di->timeout, 1024);	
    if (daveDebug & daveDebugInitAdapter) {
	    LOG3("%s initAdapter() %d chars.\n", di->name, res);
	    _daveDump("got:",b1,res);
    }	    
* /    
    return 0;
}
*/

/*
    It seems to be better to complete this subroutine even if answers
    from adapter are not as expected.
*/
int DECL2 _daveDisconnectAdapterMPI(daveInterface * di) {
    int res;
    uc m2[]={
        1,4,2
    };
    uc b1[daveMaxRawLen];
    if (daveDebug & daveDebugInitAdapter) 
	LOG2("%s enter DisconnectAdapter()\n", di->name);	
    _daveSendSingle(di, STX);
    res=_daveReadMPI(di,b1);
/*    if ((res!=1)||(b1[0]!=DLE)) return -1; */
    _daveSendWithCRC(di, m2, sizeof(m2));		
    if (daveDebug & daveDebugInitAdapter) 
	LOG2("%s daveDisconnectAdapter() step 1.\n", di->name);	
    res=_daveReadMPI(di, b1);
/*    if ((res!=1)||(b1[0]!=DLE)) return -2; */
    res=_daveReadMPI(di, b1);
/*    if ((res!=1)||(b1[0]!=STX)) return -3; */
    if (daveDebug & daveDebugInitAdapter) 
	LOG2("%s daveDisconnectAdapter() step 2.\n", di->name);	
    _daveSendSingle(di, DLE);
    _daveReadChars(di, b1, tmo_normal, daveMaxRawLen);
    _daveSendSingle(di, DLE);
    if (daveDebug & daveDebugInitAdapter) 
	_daveDump("got",b1,10);
    return 0;	
}

/*
    This doesn't work yet. I'm not sure whether it is possible to get that
    list after having connected to a PLC.
*/
int DECL2 _daveListReachablePartnersMPI(daveInterface * di,char * buf) {
    uc b1[daveMaxRawLen];
    uc m1[]={1,7,2};
    int res;
    _daveSendSingle(di,STX);
    res=_daveReadMPI(di, b1);
    if ((res!=1)||(b1[0]!=DLE)) return -1;
    
    _daveSendWithCRC(di, m1, sizeof(m1));
    res=_daveReadMPI(di,b1);
    if ((res!=1)||(b1[0]!=DLE)) return -1;	
    res=_daveReadMPI(di,b1);
    if ((res!=1)||(b1[0]!=STX)) return -1;

    _daveSendSingle(di,DLE);
    res=_daveReadMPI(di,b1);
    _daveSendSingle(di,DLE);
    memcpy(buf,b1+6,126);
    return 126;
}   

int DECL2 _daveDisconnectPLCMPI(daveConnection * dc)
{
    int res;
    uc m[]={
        0x80
    };
    uc b1[daveMaxRawLen];
/*    i=_daveSendDialog(dc, m, sizeof(m)); */
    _daveSendSingle(dc->iface, STX);
    
    res=_daveReadMPI(dc->iface,b1);
    if ((res!=1)||(b1[0]!=DLE)) {
	if (daveDebug & daveDebugPrintErrors)
    	    LOG2("%s *** no DLE before send.\n", dc->iface->name);	    
	return -1;
    }
    _daveSendWithPrefix(dc, m, 1);	
    
    res=_daveReadMPI(dc->iface,b1);
    if ((res!=1)||(b1[0]!=DLE)) {
	if (daveDebug & daveDebugPrintErrors)
	    LOG2("%s *** no DLE after send.\n", dc->iface->name);	    
	return -2;
    }    
    
    _daveSendSingle(dc->iface, DLE);
    
    res=_daveReadMPI(dc->iface,b1);
    if ((res!=1)||(b1[0]!=STX)) return 6;
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveDisConnectPLC() step 6.\n", dc->iface->name);	
/*    _daveReadMessage(dc->iface,b1,tmo_normal); */
    res=_daveReadMPI(dc->iface,b1);
    if (daveDebug & daveDebugConnect) 
	_daveDump("got",b1,10);
    _daveSendSingle(dc->iface, DLE);
    return 0;
}    

/*
    build the PDU for a PDU length negotiation    
*/
void DECL2 _daveNegPDUlengthRequest(PDU *p) {
    uc pa[]=	{0xF0, 0 ,0x00,0x01,0x00,0x01,0x03,0xC0,};
    _daveInitPDUheader(p,1);
    _daveAddParam(p, pa, sizeof(pa));
    if (daveDebug & daveDebugPDU) {
	_daveDumpPDU(p);
    }	
}    

/* 
    Open connection to a PLC. This assumes that dc is initialized by
    daveNewConnection and is not yet used.
    (or reused for the same PLC ?)
*/
int DECL2 _daveConnectPLCMPI2(daveConnection * dc) {
    int res;
    PDU p1,p2;
    uc b1[daveMaxRawLen];
    
    uc b4[]={
	0x00,0x0d,0x00,0x03,0xe0,0x04,0x00,0x80,
	0x00,0x02,0x01,0x06,
	0x01,
	0x00,
	0x00,0x01,
	0x02,0x03,0x01,0x00
           /*^^ MPI ADDR */
/*	0x04,0x80,0x80,0x0D,0x00,0x14,0xE0,0x04, */
/*	0x00,0x80,0x00,0x02,			*/
/*	0x00,			*/
/*	0x02,		*/
/*	0x01,0x00, */
/*	0x01,0x00, */
	};

    us t4[]={
	0x00,0x0c,0x103,0x103,0xd0,0x04,0x00,0x80,
	0x01,0x06,
	0x00,0x02,0x00,0x01,0x02,
	0x03,0x01,0x00,
	0x01,0x00,0x10,0x03,0x4d
    };
    uc b5[]={	
	0x05,0x01,
    };
    
    us t5[]={    
	0x00,
	0x0c,
	0x103,0x103,0x05,0x01,0x10,0x03,0x1b/*,0x10,0x03,*/
	/* 0x08 */
    };

    b4[3]=dc->connectionNumber; // 1/10/05 trying Andrew's patch
    b4[sizeof(b4)-3]=dc->MPIAdr;	
/*    b4[1]|=dc->MPIAdr;	*/
/*    t4[1]|=dc->MPIAdr;  */ /* Andrew threw it out, wrong position */
/*    t4[2]=dc->MPIAdr;  */ /* this is the position in Andrew's protocol, when MPI ist not 3, I need it. */
    t4[15]=dc->MPIAdr;
/*    t4[sizeof(t4)-1]^=dc->MPIAdr; */ /* 'patch' the checksum	*/
    t4[sizeof(t4)/2-1]^=dc->MPIAdr; /* 'patch' the checksum	*/

/*
    t5[1]|=dc->MPIAdr;	
    t5[sizeof(t5)-1]^=dc->MPIAdr;	
        
    //_daveSendSingle(dc->iface, DLE);//my
*/    
    _daveSendSingle(dc->iface, STX);
//    usleep(40000);
    res = _daveReadChars(dc->iface, b1, tmo_normal, daveMaxRawLen);
/*    LOG1("daveConnectPLC(Version 2)\n"); */
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLC(2) step 1.\n", dc->iface->name);
    _daveSendWithCRC(dc->iface, b4, sizeof(b4));
    res = _daveReadChars(dc->iface, b1, tmo_normal, daveMaxRawLen);

    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLC() step 2.\n", dc->iface->name);
    res = _daveReadChars(dc->iface, b1, tmo_normal, daveMaxRawLen);
//    usleep (40000);
    _daveSendSingle(dc->iface, DLE);
    res=_daveReadMPI2(dc->iface,b1);
    if (_daveMemcmp(t4, b1, res)) return 3;
    dc->connectionNumber2=b1[3]; // 1/10/05 trying Andrew's patch
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLC() step 3.\n", dc->iface->name);	
    res=_daveReadMPI(dc->iface,b1);
    if ((res!=1)||(b1[0]!=DLE)) return 4;

    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLC() step 4.\n", dc->iface->name);	
    _daveSendWithPrefix(dc, b5, sizeof(b5));		
    res=_daveReadMPI(dc->iface,b1);
    if ((res!=1)||(b1[0]!=DLE)) return 5;
    res=_daveReadMPI(dc->iface,b1);
    if ((res!=1)||(b1[0]!=STX)) return 5;
    
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLC() step 5.\n", dc->iface->name);	
    _daveSendSingle(dc->iface, DLE);
    
    res=_daveReadMPI2(dc->iface,b1);
    if (_daveMemcmp(t5, b1, res)) return 6;
    
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLC() step 6.\n", dc->iface->name);	
    
    dc->messageNumber=0;	
    p1.header=dc->msgOut+dc->PDUstartO;
    _daveNegPDUlengthRequest(&p1);
    res=_daveExchangeMPI(dc,&p1);
    _daveSetupReceivedPDU(dc, &p2);
    dc->maxPDUlength=daveGetU16from(p2.param+6);
    if (daveDebug & daveDebugConnect) {
	LOG2("\n*** Partner offered PDU length: %d\n\n",dc->maxPDUlength);
    }	
    return 0;
}

/* 
    Open connection to a PLC. This assumes that dc is initialized by
    daveNewConnection and is not yet used.
    (or reused for the same PLC ?)
*/
int DECL2 _daveConnectPLCMPI1(daveConnection * dc) {
    int res;
    PDU p2,p1;
    uc b4[]={
	0x04,0x80,0x80,0x0D,0x00,0x14,0xE0,0x04,
	0x00,0x80,0x00,0x02,
	0x00,
	0x02,
	0x01,0x00,
	0x01,0x00,
	};
    us t4[]={
/*	0x04,0x80,0x80,0x0C,0x14,0x03,0xD0,0x04, */
//	0x04,0x80,0x180,0x0C,0x14,0x03,0xD0,0x04,	// 1/10/05 trying Andrew's patch
	0x04,0x80,0x180,0x0C,0x114,0x103,0xD0,0x04,	// 1/10/05 trying Andrew's patch
	0x00,0x80,
	0x00,0x02,0x00,0x02,0x01,
	0x00,0x01,0x00,
    };
    uc b5[]={	
	0x05,0x01,
    };
    us t5[]={    
	0x04,
	0x80,
	0x180,0x0C,0x114,0x103,0x05,0x01,
    };
    b4[1]|=dc->MPIAdr;	
//    b4[2]|=dc->iface->localMPI;
    b4[5]=dc->connectionNumber; // 1/10/05 trying Andrew's patch
    
    t4[1]|=dc->MPIAdr;	
//    t4[2]|=dc->iface->localMPI;	
    
    t5[1]|=dc->MPIAdr;	
//    t5[2]|=dc->iface->localMPI;	
/*    LOG1("daveConnectPLC(Version 1)\n"); */
    _daveSendSingle(dc->iface, STX);
    
    _daveInitStep(dc->iface, 1, b4, sizeof(b4),"connectPLC(1)");
    
    res= _daveReadMPI2(dc->iface,dc->msgIn);
    if (_daveMemcmp(t4, dc->msgIn, sizeof(t4)/2)) return 3;
    dc->connectionNumber2=dc->msgIn[5]; // 1/10/05 trying Andrew's patch
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLC(1) step 4.\n", dc->iface->name);	
    
    if (_daveReadSingle(dc->iface)!=DLE) return 4;
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLC() step 5.\n", dc->iface->name);	
    _daveSendWithPrefix(dc, b5, sizeof(b5));		
    if (_daveReadSingle(dc->iface)!=DLE) return 5;
    if (_daveReadSingle(dc->iface)!=STX) return 5;
    
    
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLC() step 6.\n", dc->iface->name);	
    _daveSendSingle(dc->iface, DLE);
    res= _daveReadMPI2(dc->iface,dc->msgIn);
    if (_daveMemcmp(t5, dc->msgIn, sizeof(t5)/2)) return 6;
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLC() step 6.\n", dc->iface->name);	
    dc->messageNumber=0;	
    p1.header=dc->msgOut+dc->PDUstartO;
    _daveNegPDUlengthRequest(&p1);
    res=_daveExchangeMPI(dc,&p1);
    _daveSetupReceivedPDU(dc, &p2);
    dc->maxPDUlength=daveGetU16from(p2.param+6);
    if (daveDebug & daveDebugConnect) {
	LOG2("\n*** Partner offered PDU length: %d\n\n",dc->maxPDUlength);
    }	
    return 0;
}

void _daveSendS7MPI(daveInterface* di, uc*b, int size) {
    uc fix[]={0x7e,0,0,0};
    uc fix2[]={0xd3,0x3d,0x7e};
    unsigned long i;
    fix[2]=size;
    fix[3]=0xff^(size);
    daveWriteFile(di->fd.wfd,fix,sizeof(fix),i);
    _daveDump("sendS7MPI",fix,4);
    daveWriteFile(di->fd.wfd,b,sizeof(size),i);
    _daveDump("sendS7MPI",b,size);
    daveWriteFile(di->fd.wfd,fix2,3,i);
    _daveDump("sendS7MPI",fix2,3);
}
/* 
    Open connection to a PLC. This assumes that dc is initialized by
    daveNewConnection and is not yet used.
    (or reused for the same PLC ?)
*/
int DECL2 _daveConnectPLCMPI3(daveConnection * dc) {
    int res;
    PDU p2,p1;
    uc b4[]={
	0x04,0x80,0x0,0x0D,0x00,0x14,0xE0,0x04,
	0x00,0x80,0x00,0x02,
	0x00,
	0x02,
	0x01,0x00,
	0x01,0x02,
	};
//    4,87,0,d,
//0,14,e0,4,
//0,80,0,2,
//net->tty:0,2,1,0,
//1,2,d3,3d,7e,
//tty->net:	
    us t4[]={
	0x04,0x80,0x180,0x0C,0x14,0x03,0xD0,0x04,
	0x00,0x80,
	0x00,0x02,0x00,0x02,0x01,
	0x00,0x01,0x00,
    };
    uc b5[]={	
	0x05,0x01,
    };
    us t5[]={    
	0x04,
	0x80,
	0x180,0x0C,0x14,0x03,0x05,0x01,
    };
    
    b4[1]|=dc->MPIAdr;	
    b4[2]|=dc->iface->localMPI;
    
    t4[1]|=dc->MPIAdr;	
    t4[2]|=dc->iface->localMPI;	
    
    t5[1]|=dc->MPIAdr;	
//    t5[2]|=dc->iface->localMPI;	
/*    LOG1("daveConnectPLC(Version 3)\n"); */
    
    _daveSendS7MPI(dc->iface, b4, sizeof(b4));
    
//    res= _daveReadMPI2(dc->iface,dc->msgIn);
    res= _daveReadS7Packet(dc->iface,dc->msgIn);
    res= _daveReadS7Packet(dc->iface,dc->msgIn);
    if (_daveMemcmp(t4, dc->msgIn, sizeof(t4)/2)) return 3;
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLCMPI3() step 3.\n", dc->iface->name);	
    
    if (_daveReadSingle(dc->iface)!=DLE) return 4;
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLCMPI3() step 4.\n", dc->iface->name);	
    _daveSendWithPrefix(dc, b5, sizeof(b5));		
    if (_daveReadSingle(dc->iface)!=DLE) return 5;
    if (_daveReadSingle(dc->iface)!=STX) return 5;
    
    
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLC() step 5.\n", dc->iface->name);	
    _daveSendSingle(dc->iface, DLE);
    res= _daveReadMPI2(dc->iface,dc->msgIn);
    if (_daveMemcmp(t5, dc->msgIn, sizeof(t5)/2)) return 6;
    if (daveDebug & daveDebugConnect) 
	LOG2("%s daveConnectPLC() step 6.\n", dc->iface->name);	

    dc->messageNumber=0;	
    p1.header=dc->msgOut+dc->PDUstartO;
    _daveNegPDUlengthRequest(&p1);
    res=_daveExchangeMPI(dc,&p1);
	
    _daveSetupReceivedPDU(dc, &p2);
    dc->maxPDUlength=daveGetU16from(p2.param+6);
    if (daveDebug & daveDebugConnect) {
	LOG2("\n*** Partner offered PDU length: %d\n\n",dc->maxPDUlength);
    }    
    return(0);
}

/*
    Protocol specific functions for ISO over TCP:
*/

#ifdef HAVE_SELECT
int DECL2 _daveReadOne(daveInterface * di, uc *b) {
	fd_set FDS;
	struct timeval t;
	FD_ZERO(&FDS);
	FD_SET(di->fd.rfd, &FDS);
	
	t.tv_sec = di->timeout / 1000000;
	t.tv_usec = di->timeout % 1000000;
/*	if (daveDebug & daveDebugByte) 
	    LOG2("timeout %d\n",di->timeout); */
	if (select(di->fd.rfd + 1, &FDS, NULL, NULL, &t) <= 0) 
	{
	    if (daveDebug & daveDebugByte) LOG1("timeout in readOne.\n");
	    return (0);
	} else {
	    return read(di->fd.rfd, b, 1);
	} 
};
#endif

#ifdef BCCWIN
int DECL2 _daveReadOne(daveInterface * di, uc *b) {
    unsigned long i;
    char res;
    ReadFile(di->fd.rfd, b, 1, &i,NULL);
    return i;
}
#endif 


#ifdef timeoutOnSingleBytes
/*
    Read one byte with interface timeout.
*/
#ifdef HAVE_SELECT

/*
    Read one complete packet. The bytes 3 and 4 contain length information.
*/
int DECL2 _daveReadISOPacket(daveInterface * di,uc *b) {
	int i,res=0,length;
	res+=_daveReadOne(di,b);
	res+=_daveReadOne(di,b+res);
	res+=_daveReadOne(di,b+res);
	res+=_daveReadOne(di,b+res);
	if ((res>0)&&(res<4)) res+=_daveReadOne(di,b+res);
	if ((res>0)&&(res<4)) res+=_daveReadOne(di,b+res);
	if ((res>0)&&(res<4)) res+=_daveReadOne(di,b+res);
        if (res<4) {
	    if (daveDebug & daveDebugByte) {
		LOG2("res %d ",res);
		_daveDump("readISOpacket: short packet", b, res);
	    }
	    return (0); /* short packet */
	}
/*
    This may be shorter, but we would hang on malformed packets.
	res+=read(fd, b+res,b[2]+5 ); //length +8 -3 already read    
    So do it byte by byte.
    Somebody might have a better solution using non-blocking something.
*/	
	length=b[3]+0x100*b[2];
	while (res<length) {
	    i=_daveReadOne(di, b+res); /* length +8 -3 already read */
	    if (i<1) {
		if (daveDebug & daveDebugByte) 
		    LOG3("readISOpacket: %d bytes read, %d needed\n",res, length);
		 return (0);
	    }    	 
	    res+=i;
	} 
	if (daveDebug & daveDebugByte) {
	    LOG3("readISOpacket: %d bytes read, %d needed\n",res, length);
	    _daveDump("readISOpacket: packet", b, res);    
	}    
	return (res);
};
#endif
#else
/*
    Read one complete packet. The bytes 3 and 4 contain length information.
    This version needs a socket filedescriptor that is set to O_NONBLOCK or
    it will hang, if there are not enough bytes to read.
    The advantage may be that the timeout is not used repeatedly.
*/
#ifdef HAVE_SELECT
int DECL2 _daveReadISOPacket(daveInterface * di,uc *b) {
	int res,length;
	fd_set FDS;
	struct timeval t;
	FD_ZERO(&FDS);
	FD_SET(di->fd.rfd, &FDS);
	
	t.tv_sec = di->timeout / 1000000;
	t.tv_usec = di->timeout % 1000000;
	if (select(di->fd.rfd + 1, &FDS, NULL, NULL, &t) <= 0) {
	    if (daveDebug & daveDebugByte) LOG1("timeout in ReadISOPacket.\n");
	    return 0;
	} else {
	res=read(di->fd.rfd, b, 4);
        if (res<4) {
	    if (daveDebug & daveDebugByte) {
		LOG2("res %d ",res);
		_daveDump("readISOpacket: short packet", b, res);
	    }
	    return (0); /* short packet */
	}
	length=b[3]+0x100*b[2];
	res+=read(di->fd.rfd, b+4, length-4);
	if (daveDebug & daveDebugByte) {
	    LOG3("readISOpacket: %d bytes read, %d needed\n",res, length);
	    _daveDump("readISOpacket: packet", b, res);    
	}
	return (res);
	}
}
#endif /* HAVE_SELECT */

#ifdef BCCWIN

int DECL2 _daveReadISOPacket(daveInterface * di,uc *b) {
	int res,i,length;
	i=recv((SOCKET)(di->fd.rfd), b, 4, 0);
	res=i;
	if (res <= 0) {
	    if (daveDebug & daveDebugByte) LOG1("timeout in ReadISOPacket.\n");
	    return 0;
	} else {
    	    if (res<4) {
	    if (daveDebug & daveDebugByte) {
		LOG2("res %d ",res);
		_daveDump("readISOpacket: short packet", b, res);
	    }
	    return (0); /* short packet */
	}
	length=b[3]+0x100*b[2];
//	res+=read(di->fd.rfd, b+4, length-4);
	i=recv((SOCKET)(di->fd.rfd), b+4, length-4, 0);
	res+=i;
	if (daveDebug & daveDebugByte) {
	    LOG3("readISOpacket: %d bytes read, %d needed\n",res, length);
	    _daveDump("readISOpacket: packet", b, res);    
	}
	return (res);
	}
}


#endif /* HAVE_SELECT */

#endif /* */

int DECL2 _daveSendISOPacket(daveConnection * dc, int size) {
    unsigned long i;
    size+=4;
    *(dc->msgOut+3)=size % 0xFF;
    *(dc->msgOut+2)=size / 0x100;
    *(dc->msgOut+1)=0;
    *(dc->msgOut+0)=3;
    if(dc->msgOut[5]==0xF0) {
	if (dc->messageNumber==0xff) {	
	    dc->msgOut[11]=0xcc;
	    dc->msgOut[12]=0xc1;
	    dc->messageNumber++;	
	}    
    } else 	
	dc->messageNumber=0xff;	
	
    if (daveDebug & daveDebugByte) 
	_daveDump("send packet: ",dc->msgOut,size);
#ifdef HAVE_SELECT
    daveWriteFile(dc->iface->fd.wfd, dc->msgOut, size, i);
#endif    
#ifdef BCCWIN
    send((unsigned int)(dc->iface->fd.wfd), dc->msgOut, size, 0);
#endif
    return 0;
}

int DECL2 _daveGetResponseISO_TCP(daveConnection * dc) {
    return _daveReadISOPacket(dc->iface,dc->msgIn);
}
/*
    Executes the dialog around one message:
*/
int DECL2 _daveExchangeTCP(daveConnection * dc, PDU * p) {
    int res;
    PDU p2;
    if (daveDebug & daveDebugExchange) {
        LOG2("%s enter _daveExchangeTCP\n", dc->iface->name);	    
    }    
    *(dc->msgOut+6)=0x80;
    *(dc->msgOut+5)=0xf0;
    *(dc->msgOut+4)=0x02;
    _daveSendISOPacket(dc,3+p->hlen+p->plen+p->dlen);
    res=_daveReadISOPacket(dc->iface,dc->msgIn);
    
    if (res>0) {
	 _daveSetupReceivedPDU(dc, &p2);
	if (daveDebug & daveDebugExchange) {
	    LOG3("%s _daveExchangeTCP got %d result bytes\n", dc->iface->name, dc->AnswLen);
	}    
	return 0;
    }
    return -1;
}

int DECL2 _daveConnectPLCTCP(daveConnection * dc) {
    int res, success, retries;
    uc b4[]={
	0x11,0xE0,0x00,
	0x00,0x00,0x01,0x00,
	0xC1,2,1,0,
	0xC2,2,0,1,
	0xC0,1,9,
    };
    uc b243[]={
	0x11,0xE0,0x00,
	0x00,0x00,0x01,0x00,
	0xC1,2,'M','W',
	0xC2,2,'M','W',
	0xC0,1,9,
    };
    
    uc negPDU[]={
	0xF0,0,0,1,
	0,1,3,0xc0,
	};	
    PDU p1,p2;	
    success=0;
    retries=0;
    do {
	if (dc->iface->protocol==daveProtoISOTCP243) {
	    memcpy(dc->msgOut+4, b243, sizeof(b243));	
	} else {
	    memcpy(dc->msgOut+4, b4, sizeof(b4));	
	    dc->msgOut[17]=dc->rack+1;
	    dc->msgOut[18]=dc->slot;
	}	
        _daveSendISOPacket(dc, sizeof(b4)); /* sizes are identical */
	res=_daveReadISOPacket(dc->iface,dc->msgIn);
        if (daveDebug & daveDebugConnect) {
	    LOG2("%s daveConnectPLC() step 1. ", dc->iface->name);	
	    _daveDump("got packet: ", dc->msgIn, res);
	}
	if (res==22) {
	    success=1;
	} else {
	    if (daveDebug & daveDebugPrintErrors){
		LOG2("%s error in daveConnectPLC() step 1. retrying...", dc->iface->name);	
	    }	
	}
	retries++;
    } while ((success==0)&&(retries<3));
    if (success==0) return -1;
    
    retries=0;
    do {
	p1.header=dc->msgOut+dc->PDUstartO;
	_daveInitPDUheader(&p1, 1);
	_daveAddParam(&p1, negPDU, sizeof(negPDU));
	if (daveDebug & daveDebugPDU) {
	    _daveDumpPDU(&p1);
	}	
	res=_daveExchange(dc, &p1);
	if (daveDebug & daveDebugConnect) {
    	    LOG3("%s daveConnectPLC() step 2. Result of exchange:%d\n", dc->iface->name,res);	
	}
	if (res==0) {
	    _daveSetupReceivedPDU(dc, &p2);
	    dc->maxPDUlength=daveGetU16from(p2.param+6);
	    if (daveDebug & daveDebugConnect) {
		LOG2("\n*** Partner offered PDU length: %d\n\n",dc->maxPDUlength);
	    }    
	    return res;
	} else {
	    if (daveDebug & daveDebugPrintErrors){
		LOG2("%s error in daveConnectPLC() step 1. retrying...", dc->iface->name);	
	    }	
	}
    } while (retries<3);	
    return -1;
}

/*
    Changes: 
    07/19/04 removed unused vars.
*/



/*
    Changes: 
    07/19/04 added return values in daveInitStep and daveSendWithPrefix2.
    09/09/04 applied patch for variable Profibus speed from Andrew Rostovtsew.
*/

/* PPI specific functions: */
#define tmo_normalPPI 140000

//#define DLE 0x10
//#define SYN 0x16

void DECL2 _daveSendLength(daveInterface * di, int len)
{
    uc c[]={104,0,0,104};
    unsigned long i;
//    int i;
    c[1]=len;
    c[2]=len;
    daveWriteFile(di->fd.wfd, c, 4, i);
    if ((daveDebug & daveDebugByte)!=0) {
	_daveDump("I send", c, 4);
    }	
}

void DECL2 _daveSendIt(daveInterface * di, uc * b, int size)
{
//    int i;
    unsigned long i;
    us sum = 0;
    for (i=0;i<size;i++) {
	sum+=b[i];
    }
    sum=sum & 0xff;
    b[size]=sum;
    size++;
    b[size]=SYN;
    size++;
    daveWriteFile(di->fd.wfd, b, size, i);
    
    if ((daveDebug & daveDebugByte)!=0) {
	LOG2("send %ld\n",i);
	_daveDump("I send", b, size);
    }	
}

void DECL2 _daveSendDATAWAIT(daveConnection * dc) {
    uc b[]={DLE,0,0,0x5C,0,0};
    unsigned long i;
    b[1]=dc->MPIAdr;
    b[2]=dc->iface->localMPI;
    daveWriteFile(dc->iface->fd.wfd, b, 1, i);
    _daveSendIt(dc->iface, b+1, sizeof(b)-3);
}

void DECL2 _daveSendYOURTURN(daveConnection * dc) {
    uc b[]={DLE,0,0,0x7C,0,0};
    unsigned long i;
    b[1]=dc->MPIAdr;
    b[2]=dc->iface->localMPI;
    daveWriteFile(dc->iface->fd.wfd, b, 1, i);
    _daveSendIt(dc->iface, b+1, sizeof(b)-3);
}


int seconds, thirds;

int _daveGetResponsePPI(daveConnection *dc) {
    int res, expectedLen, expectingLength, i, sum, myTurn;
    uc * b;
    res = 0;
    expectedLen=6;
    expectingLength=1;
    b=dc->msgIn;
    myTurn=1;
    while ((expectingLength)||(res<expectedLen)) {
	i = _daveReadChars(dc->iface, dc->msgIn+res, 2000000, daveMaxRawLen);
	res += i;
	if ((daveDebug & daveDebugByte)!=0) {
	    LOG3("i:%d res:%d\n",i,res);
	    FLUSH;
	}        
	if (i == 0) {
//    		    return 512
	    ;
	} else {
	    if ( (expectingLength) && (res==1) && (b[0] == 0xE5)) {
		if(myTurn) {
		    _daveSendYOURTURN(dc);
		    res=0;
		    myTurn=0;
		} else {
		    _daveSendDATAWAIT(dc);
		    res=0;
		    myTurn=1;
		}
	    }
	    if ( (expectingLength) && (res>=4) && (b[0] == b[3]) && (b[1] == b[2]) ) {
	        expectedLen=b[1]+6;
	        expectingLength=0;
	    }
	}	
    }
    if ((daveDebug & daveDebugByte)!=0) {
	LOG2("res %d testing lastChar\n",res);
    }	
    if (b[res-1]!=SYN) {
    	LOG1("block format error\n");
    	return 1024;
    }
    if ((daveDebug & daveDebugByte)!=0) {
	LOG1("testing check sum\n");
    }	
    sum=0;
    for (i=4; i<res-2; i++){
        sum+=b[i];
    }
    sum=sum&0xff;
    if ((daveDebug & daveDebugByte)!=0) {
	LOG3("I calc: %x sent: %x\n", sum, b[res-2]);
    }	
    if (b[res-2]!=sum) {
	if ((daveDebug & daveDebugByte)!=0) {
    	    LOG1("checksum error\n");
	}	
        return 2048;
    }
    return 0;
} 

int DECL2 _daveExchangePPI(daveConnection * dc,PDU * p1) {
    int i,res=0,len, myTurn;
    dc->msgOut[0]=dc->MPIAdr;	/* address ? */
    dc->msgOut[1]=dc->iface->localMPI;	
    dc->msgOut[2]=108;	
    len=3+p1->hlen+p1->plen+p1->dlen;	/* The 3 fix bytes + all parts of PDU */
    _daveSendLength(dc->iface, len);			
    _daveSendIt(dc->iface, dc->msgOut, len);
//    i = _daveReadChars(dc->iface, dc->msgIn+res, tmo_normalPPI, daveMaxRawLen);
    i = _daveReadChars(dc->iface, dc->msgIn+res, tmo_normalPPI, 1);
    if ((daveDebug & daveDebugByte)!=0) {
	LOG3("i:%d res:%d\n",i,res);
	_daveDump("got",dc->msgIn,res); // 5.1.2004
    }	
    if (i == 0) {
	seconds++;
	_daveSendLength(dc->iface, len);			
	_daveSendIt(dc->iface, dc->msgOut, len);
	i = _daveReadChars(dc->iface, dc->msgIn+res, 2*tmo_normalPPI, daveMaxRawLen);
	if (i == 0) {
	    thirds++;
	    _daveSendLength(dc->iface, len);			
	    _daveSendIt(dc->iface, dc->msgOut, len);
	    i = _daveReadChars(dc->iface, dc->msgIn+res, 4*tmo_normalPPI, daveMaxRawLen);
	    if (i == 0) {
		LOG1("timeout in _daveExchangePPI!\n");
		FLUSH;
    		return 512;
	    }	
	}    
    }
    _daveSendDATAWAIT(dc); 
    myTurn=1;
    return _daveGetResponsePPI(dc);
/*    
    res = 0;
    while ((expectingLength)||(res<expectedLen)) {
//	i = _daveReadChars(dc->iface, dc->msgIn+res, 2*tmo_normalPPI, daveMaxRawLen);
	i = _daveReadChars(dc->iface, dc->msgIn+res, 2*tmo_normalPPI, 1);
	res += i;
	if ((daveDebug & daveDebugByte)!=0) {
	    LOG3("i:%d res:%d\n",i,res);
	    _daveDump("got",dc->msgIn,res);  // 5.1.2004
	    FLUSH;
	}        
	if (i == 0) {
    	    return 512;
	} else {
	    if ( (expectingLength) && (res==1) && (b[0] == 0xE5)) {
		if(myTurn) {
		    _daveSendYOURTURN(dc);
		    res=0;
		    myTurn=0;
		} else {
		    _daveSendDATAWAIT(dc);
		    res=0;
		    myTurn=1;
		}
	    }
	    if ( (expectingLength) && (res>=4) && (b[0] == b[3]) && (b[1] == b[2]) ) {
		expectedLen=b[1]+6;
		expectingLength=0;
	    }
	}     
    }
    if ((daveDebug & daveDebugByte)!=0) {
	LOG2("res %d testing lastChar\n",res);
    }	
    if (b[res-1]!=SYN) {
        LOG1("block format error\n");
        return 1024;
    }
    if ((daveDebug & daveDebugByte)!=0) {
	LOG1("testing check sum\n");
    }	
    sum=0;
    for (i=4; i<res-2; i++){
        sum+=b[i];
    }
    sum=sum&0xff;
    if ((daveDebug & daveDebugByte)!=0) {
	LOG3("I calc: %x sent: %x\n", sum, b[res-2]);
    }	
    if (b[res-2]!=sum) {
	if ((daveDebug & daveDebugByte)!=0) {
    	    LOG1("checksum error\n");
	}	
        return 2048;
    }
    return 0;
*/    
}    
/* 
    "generic" functions calling the protocol specific ones (or the dummies)
*/

int DECL2 daveInitAdapter(daveInterface * di) {
    return di->initAdapter(di);
}

int DECL2 daveConnectPLC(daveConnection * dc) {
    return dc->iface->connectPLC(dc);
}

int DECL2 daveDisconnectPLC(daveConnection * dc) {
    return dc->iface->disconnectPLC(dc);
}

int DECL2 daveDisconnectAdapter(daveInterface * di) {
    return di->disconnectAdapter(di);
}

int DECL2 _daveExchange(daveConnection * dc, PDU *p) {
    return dc->iface->exchange(dc, p);
}

int DECL2 daveSendMessage(daveConnection * dc, PDU *p) {
    return dc->iface->sendMessage(dc, p);
}

int DECL2 daveListReachablePartners(daveInterface * di, char * buf) {
    return di->listReachablePartners(di, buf);
}

int DECL2 daveGetResponse(daveConnection * dc) {
    return dc->iface->getResponse(dc);
}

/**
    Newer conversion routines. As the terms WORD, INT, INTEGER etc have different meanings
    for users of different programming languages and compilers, I choose to provide a new 
    set of conversion routines named according to the bit length of the value used. The 'U'
    or 'S' stands for unsigned or signed.
**/
/*
    Get a value from the position b points to. B is typically a pointer to a buffer that has
    been filled with daveReadBytes:
*/
EXPORTSPEC int DECL2 daveGetS8from(uc *b) {
    char* p=(char*)b;
    return *p;
}

EXPORTSPEC int DECL2 daveGetU8from(uc *b) {
    return *b;
}

EXPORTSPEC int DECL2 daveGetS16from(uc *b) {
    union {
	short a;
	uc b[2];
    } u;
    u.b[1]=*b;
    b++;
    u.b[0]=*b;
    return u.a;
}

EXPORTSPEC int DECL2 daveGetU16from(uc *b) {
    union {
	unsigned short a;
	uc b[2];
    } u;
    u.b[1]=*b;
    b++;
    u.b[0]=*b;
    return u.a;
}

EXPORTSPEC int DECL2 daveGetS32from(uc *b) {
    union {
	int a;
	uc b[4];
    } u;
    u.b[3]=*b;
    b++;
    u.b[2]=*b;
    b++;
    u.b[1]=*b;
    b++;
    u.b[0]=*b;
    return u.a;
}
EXPORTSPEC unsigned int DECL2 daveGetU32from(uc *b) {
    union {
	unsigned int a;
	uc b[4];
    } u;
    u.b[3]=*b;
    b++;
    u.b[2]=*b;
    b++;
    u.b[1]=*b;
    b++;
    u.b[0]=*b;
    return u.a;
}

EXPORTSPEC float DECL2 daveGetFloatfrom(uc *b) {
    union {
	float a;
	uc b[4];
    } u;
    u.b[3]=*b;
    b++;
    u.b[2]=*b;
    b++;
    u.b[1]=*b;
    b++;
    u.b[0]=*b;
    return u.a;
}


/*
    Get a value from the current position in the last result read on the connection dc.
    This will increment an internal pointer, so the next value is read from the position
    following this value.
*/
EXPORTSPEC int DECL2 daveGetS8(daveConnection * dc) {
    char * p=(char *) dc->resultPointer;
    dc->resultPointer++;
    return *p;
}

EXPORTSPEC int DECL2 daveGetU8(daveConnection * dc) {
    uc * p=dc->resultPointer;
    dc->resultPointer++;
    return *p;
}    

EXPORTSPEC int DECL2 daveGetS16(daveConnection * dc) {
    union {
	short a;
	uc b[2];
    } u;
    u.b[1]=*(dc->resultPointer);
    dc->resultPointer++;
    u.b[0]=*(dc->resultPointer);
    dc->resultPointer++;
    return u.a;
}

EXPORTSPEC int DECL2 daveGetU16(daveConnection * dc) {
    union {
	unsigned short a;
	uc b[2];
    } u;
    u.b[1]=*(dc->resultPointer);
    dc->resultPointer++;
    u.b[0]=*(dc->resultPointer);
    dc->resultPointer++;
    return u.a;
}

EXPORTSPEC int DECL2 daveGetS32(daveConnection * dc) {
    union {
	int a;
	uc b[4];
    } u;
    u.b[3]=*(dc->resultPointer);
    dc->resultPointer++;
    u.b[2]=*(dc->resultPointer);
    dc->resultPointer++;
    u.b[1]=*(dc->resultPointer);
    dc->resultPointer++;
    u.b[0]=*(dc->resultPointer);
    dc->resultPointer++;
    return u.a;
}

EXPORTSPEC unsigned int DECL2 daveGetU32(daveConnection * dc) {
    union {
	unsigned int a;
	uc b[4];
    } u;
    u.b[3]=*(dc->resultPointer);
    dc->resultPointer++;
    u.b[2]=*(dc->resultPointer);
    dc->resultPointer++;
    u.b[1]=*(dc->resultPointer);
    dc->resultPointer++;
    u.b[0]=*(dc->resultPointer);
    dc->resultPointer++;
    return u.a;
}
/*
    Get a value from a given position in the last result read on the connection dc.
*/
EXPORTSPEC int DECL2 daveGetS8at(daveConnection * dc, int pos) {
    char * p=(char *)(dc->_resultPointer);
    p+=pos;
    return *p;
}

EXPORTSPEC int DECL2 daveGetU8at(daveConnection * dc, int pos)  {
    uc * p=(uc *)(dc->_resultPointer);
    p+=pos;
    return *p;
}

EXPORTSPEC int DECL2 daveGetS16at(daveConnection * dc, int pos) {
    union {
	short a;
	uc b[2];
    } u;
    uc * p=(uc *)(dc->_resultPointer);
    p+=pos;
    u.b[1]=*p;
    p++;
    u.b[0]=*p;
    return u.a;
}

EXPORTSPEC int DECL2 daveGetU16at(daveConnection * dc, int pos) {
    union {
	unsigned short a;
	uc b[2];
    } u;
    uc * p=(uc *)(dc->_resultPointer);
    p+=pos;
    u.b[1]=*p;
    p++;
    u.b[0]=*p;
    return u.a;
}

EXPORTSPEC int DECL2 daveGetS32at(daveConnection * dc, int pos) {
    union {
	int a;
	uc b[4];
    } u;
    uc * p=dc->_resultPointer;
    p+=pos;
    u.b[3]=*p;
    p++;
    u.b[2]=*p;
    p++;
    u.b[1]=*p;
    p++;
    u.b[0]=*p;
    return u.a;
}

EXPORTSPEC unsigned int DECL2 daveGetU32at(daveConnection * dc, int pos) {
    union {
	unsigned int a;
	uc b[4];
    } u;
    uc * p=(uc *)(dc->_resultPointer);
    p+=pos;
    u.b[3]=*p;
    p++;
    u.b[2]=*p;
    p++;
    u.b[1]=*p;
    p++;
    u.b[0]=*p;
    return u.a;
}
/*
    put one byte into buffer b:
*/
EXPORTSPEC uc * DECL2 davePut8(uc *b,int v) {
    *b = v & 0xff;
    *b++;
    return b;
}

EXPORTSPEC uc * DECL2 davePut16(uc *b,int v) {
    union {
	short a;
	uc b[2];
    } u;
    u.a=v;
    *b=u.b[1];
    b++;
    *b=u.b[0];
    b++;
    return b;
}

EXPORTSPEC uc * DECL2 davePut32(uc *b, int v) {
    union {
	int a;
	uc b[2];
    } u;
    u.a=v;
    *b=u.b[3];
    b++;
    *b=u.b[2];
    b++;
    *b=u.b[1];
    b++;
    *b=u.b[0];
    b++;
    return b;
}

EXPORTSPEC uc * DECL2 davePutFloat(uc *b,float v) {
    union {
	float a;
	uc b[2];
    } u;
    u.a=v;
    *b=u.b[3];
    b++;
    *b=u.b[2];
    b++;
    *b=u.b[1];
    b++;
    *b=u.b[0];
    b++;
    return b;
}

EXPORTSPEC void DECL2 davePut8at(uc *b, int pos, int v) {
    union {
	short a;
	uc b[2];
    } u;
    u.a=v;
    b+=pos;
    *b=v & 0xff;
}

EXPORTSPEC void DECL2 davePut16at(uc *b, int pos, int v) {
    union {
	short a;
	uc b[2];
    } u;
    u.a=v;
    b+=pos;
    *b=u.b[1];
    b++;
    *b=u.b[0];
}

EXPORTSPEC void DECL2 davePut32at(uc *b, int pos, int v) {
    union {
	int a;
	uc b[2];
    } u;
    u.a=v;
    b+=pos;
    *b=u.b[3];
    b++;
    *b=u.b[2];
    b++;
    *b=u.b[1];
    b++;
    *b=u.b[0];
}

EXPORTSPEC void DECL2 davePutFloatat(uc *b, int pos,float v) {
    union {
	float a;
	uc b[2];
    } u;
    u.a=v;
    b+=pos;
    *b=u.b[3];
    b++;
    *b=u.b[2];
    b++;
    *b=u.b[1];
    b++;
    *b=u.b[0];
}

userReadFunc readCallBack=NULL;
userWriteFunc writeCallBack=NULL;

void _daveConstructReadResponse(PDU * p) {
    uc pa[]={4,1}; 
    uc da[]={0xFF,4,0,0}; 
    _daveInitPDUheader(p,3);
    _daveAddParam(p, pa, sizeof(pa));
    _daveAddData(p, da, sizeof(da));    
}

void _daveConstructBadReadResponse(PDU * p) {
    uc pa[]={4,1}; 
    uc da[]={0x0A,0,0,0}; 
    _daveInitPDUheader(p,3);
    _daveAddParam(p, pa, sizeof(pa));
    _daveAddData(p, da, sizeof(da));    
}

void _daveConstructWriteResponse(PDU * p) {
    uc pa[]={5,1}; 
    uc da[]={0xFF}; 
    _daveInitPDUheader(p,3);
    _daveAddParam(p, pa, sizeof(pa));
    _daveAddData(p, da, sizeof(da));    
}

void _daveHandleRead(PDU * p1,PDU * p2) {
    int result;
    uc * userBytes;
    int bytes=0x100*p1->param[6]+p1->param[7];
    int DBnumber=0x100*p1->param[8]+p1->param[9];
    int area=p1->param[10];
    int start=0x10000*p1->param[11]+0x100*p1->param[12]+p1->param[13];
    LOG5("read %d bytes from %s %d beginning at %d.\n",
	bytes, daveAreaName(area),DBnumber,start);
    if (readCallBack)	
	userBytes=readCallBack(area, DBnumber,start, bytes, &result);	
    _daveConstructReadResponse(p2);	
    _daveAddValue(p2, userBytes, bytes);
    _daveDumpPDU(p2);
};

void _daveHandleWrite(PDU * p1,PDU * p2) {
    int result,bytes=0x100*p1->param[6]+p1->param[7];
    int DBnumber=0x100*p1->param[8]+p1->param[9];
    int area=p1->param[10];
    int start=0x10000*p1->param[11]+0x100*p1->param[12]+p1->param[13];
    LOG5("write %d bytes to %s %d beginning at %d.\n",
	bytes, daveAreaName(area),DBnumber,start);
    if (writeCallBack)	
	writeCallBack(area, DBnumber,start, bytes, &result, p1->data+4);	
    LOG1("after callback\n");
    FLUSH;
    _daveConstructWriteResponse(p2);	
    LOG1("after ConstructWriteResponse()\n");
    FLUSH;
    _daveDumpPDU(p2);
    LOG1("after DumpPDU()\n");
    FLUSH;
};


/*
    10/04/2003 	PPI has an address. Implemented now.
    06/03/2004 	Fixed a bug in _davePPIexchange, which caused timeouts
		when the first call to readChars returned less then 4 characters.
*/

/*
    Changes: 
    09/09/04 applied patch for variable Profibus speed from Andrew Rostovtsew.
    12/09/04 removed debug printf from daveConnectPLC.
    12/09/04 found and fixed a bug in daveFreeResults(): The result set is provided by the 
	     application and not necessarily dynamic. So we shall not free() it.
    12/10/04 added single bit read/write functions.
    12/12/04 added Timer/Counter read functions.
    12/13/04 changed dumpPDU to dump multiple results from daveFuncRead
    12/15/04 changed comments to pure C style 
    12/15/04 replaced calls to write() with makro daveWriteFile.
    12/15/04 removed daveSendDialog. Was only used in 1 place.
    12/16/04 removed daveReadCharsPPI. It is replaced by daveReadChars.
    12/30/04 Read Timers and Counters from 200 family. These are different internal types!
    01/02/05 Hopefully fixed local MPI<>0.
    01/10/05 Fixed some debug levels in connectPLCMPI
    01/10/05 Splitted daveExchangeMPI into the send and receive parts. They are separately
	     useable when communication is initiated by PLC.
    01/10/05 Code cleanup. Some more things in connectPLC can be done using genaral
	     MPI communication subroutines.
    01/10/05 Partially applied changes from Andrew Rostovtsew for multiple MPI connections
	     over the same adapter.
    01/11/05 Lasts steps in connect PLC can be done with exchangeMPI.
    01/26/05 replaced _daveConstructReadRequest by the sequence prepareReadRequest, addVarToReadRequest
    01/26/05 added multiple write
*/
