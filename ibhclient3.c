/*
 Part of Libnodave, a free communication libray for Siemens S7 300/400.
 This version uses the IBHLink MPI-Ethernet-Adapter from IBH-Softec.
 www.ibh.de
 
 (C) Thomas Hergenhahn (thomas.hergenhahn@web.de) 2002.

 Libnodave is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 Libnodave is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Libnodave; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  
*/

#include <stdio.h>
#include <unistd.h>

#define ThisModule "IBHClient: "
#define uc unsigned char

#include <sys/time.h>
//#include <sys/socket.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
//#include <fcntl.h>
//#include <byteswap.h>

#include "nodaveIBH.h"

//#define DONT_USE_GETHOSTBYNAME

#define bSize 1256
#define us unsigned short

#include <sys/ioctl.h>

//#ifndef DONT_USE_GETHOSTBYNAME
//#include <netdb.h>
//#endif    

#include <string.h>

#include "log.h"

#include "openSocket.h"
#define debug 10    

uc _MPIack[]={
	0x07,0xff,0x08,0x05,0x00,0x00,0x82,0x00, 0x15,0x14,0x02,0x00,0x03,0xb0,0x01,0x00,
};

#define davePtEmpty -2
#define davePtMPIAck -3
#define davePtUnknownMPIFunc -4
#define davePtUnknownPDUFunc -5
#define davePtReadResponse 1
#define davePtWriteResponse 2

int PID;

int main(int argc, char **argv) {
    _daveOSserialType fds;
    daveInterface * di;
    daveConnection * dc;
    struct timeval t1, t2;
    int log, netFd, port, ki;
    FILE * logFile;
    char * peer;
    char * portN;
    double usec;
    uc buffer[bSize], *b;
//    uc test[]={0x71,0x72,0x73,0x24,0x25};
//    daveDebug=daveDebugAll &~daveDebugByte &~daveDebugPacket;
//    daveDebug=0;
    PID=getpid();
    daveDebug=daveDebugAll;
    log=0;
    if (argc<2) {
	printf("Usage: ibhclient host:port %d\n",argc);
	printf("Example with NetLink: ibhclient 192.168.17.110 1099\n");
	printf("Example with simulator: localhost 1099\n");
	return -1;
    }
    if (argc>=4) {
	logFile = fopen(argv[3],"w+");
	if (logFile==NULL) {
	    printf("Could not open log file!\n");
	    return -1;
	}
	log=1;
	printf("Logging to %s\n",argv[3]);
    }	
    
    peer=argv[1];
    portN=argv[2];
    port = atol(portN);
    LOG3(ThisModule "host: %s port %d\n", peer,port);
    netFd=openSocket(port, peer);
    if (netFd<=0) {
	printf("Could not connect to host!\n");
	return -1;
    }
    LOG2(ThisModule "netFd: %d\n", netFd);
    fds.rfd=netFd;
    fds.wfd=netFd;
    di=daveNewInterface(fds, "IBH", 0, 0, daveSpeed187k);
    dc=daveNewConnection(di, 2, 0, 0);

    b =buffer;
    
    daveConnectPLC_IBH(dc);

    gettimeofday(&t1, NULL);

    for (ki=0;ki<1000;ki++) {    
//	daveWriteBytesIBH(dc, daveDB, 40, 0, 5, &test);
	daveReadBytesIBH(dc, daveFlags, 0, 0, 100, NULL);
	_daveDump("MW0..:",dc->_resultPointer,10);
	daveReadBytesIBH(dc, daveDB, 11, 0, 100, NULL);
	_daveDump("DB11:",(uc*)(dc->_resultPointer),dc->AnswLen);
    } 

    gettimeofday(&t2, NULL);
    usec = (t2.tv_sec - t1.tv_sec) + (t2.tv_usec - t1.tv_usec)*1e-6;
    printf(" %g sec.\n", usec);
//    FLUSH;
    daveDisconnectPLC_IBH(dc);
    return 0;
    
}

/*
    Changes: 
    07/19/04 removed unused vars.
    09/09/04 applied patch for variable Profibus speed from Andrew Rostovtsew.
    09/09/04 removed unused includes byteswap.h, sys/socket.h, netdb.h
*/
